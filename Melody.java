import java.util.*;
import java.io.*;
import java.awt.*;
import java.awt.image.*;
import javax.imageio.*;
public class Melody{
	public static void main(String[]args){
		try{
			c.U.imp("Inicio");
			String nombre="C:\\Desarrollo\\Audio\\wavs\\Elkin02";
			Wave w=new Wave(new File(nombre+".wav"));
			double[]datos=w.getms();
			double[]s=new double[datos.length];
			int ni=43;
			int nf=69;
			int dt=20;
			int na=10;
			ArrayList<Masa>ms=Masa.rango(ni,nf,dt,1);
			int l=5000;
			int ai=-1;
			ArrayList<Masa>am=null;
			for(int i=0;i<datos.length-l;i+=l){
				Masa.resx(ms);
				Masa.coe=1;
				Masa.g(datos,ms,i,i+l);
				Masa.coe=0.995;
				int im=Masa.max(ms);
				if(im>0&&i<datos.length-1){
					if(im==ai){
						Masa.coe=1;
						Masa.g(datos,s,am,i,i+l);
						Masa.coe=0.995;
					}else{
						ArrayList<Masa>nz=Masa.rango(ms.get(im).n,ms.get(im).n,0,na);
						Masa.coe=1;
						Masa.g(datos,s,nz,i,i+l);
						Masa.coe=0.995;
						if(am!=null){
							Masa.z(s,am,i,i+l);
						}
						am=nz;
					}
				}else{
					if(am!=null){
						Masa.z(s,am,i,i+l);
					}
				}
				ai=im;
			}
			//Wave.p(WGen.sumar(s,0.0001,datos,1,0),nombre+"-Melody"+ni+"_"+nf+"_"+dt+"_"+na+".wav");
			Wave.p(s,nombre+"-Melody"+ni+"_"+nf+"_"+dt+"_"+na+".wav");
			c.U.imp("Fin");
		}catch(Exception e){
			c.U.imp(e);
		}
	}
}