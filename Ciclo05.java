import java.io.*;
import java.util.*;
public class Ciclo05{
	public static void main(String[]argumentos){
		try{
			Masa.coe=1;
			System.out.println("Inicio");
			c.U.imp("Inicio");
			String nombre="D:\\Wavs\\Seek and Destroy-Bass2";
			int ni=26;
			int nf=64;
			int l=1000;
			Wave w=new Wave(new File(nombre+".wav"));
			double[]datos=w.getms(0,44100*60);
			ArrayList<int[]>as=WGen.amplitudes(datos);
			//double[]dss=WGen.toDatos(datos.length,as);
			as=WGen.simplificarAmplitudes(as,l);
			HashMap<Integer,Integer>h=new HashMap<>();
			int tini=-1;
			int tfin=-1;
			int val=0;
			ArrayList<int[]>evs=new ArrayList<>();
			for(int i=0;i<as.size();i++){
				if(as.get(i)[1]<=l){
					continue;
				}
				for(int j=i+1;j<as.size()&&WGen.gn(44100./(as.get(j)[0]-as.get(i)[0]))>=ni-1;j++){
					if(as.get(j)[1]<=l){
						continue;
					}
					if(WGen.gn(44100./(as.get(j)[0]-as.get(i)[0]))>nf+1){
						continue;
					}
					int n=(int)Math.round(WGen.gn(44100./(as.get(j)[0]-as.get(i)[0])));
					for(int k=j+1;k<as.size()&&WGen.gn(44100./(as.get(k)[0]-as.get(j)[0]))>=ni-1;k++){
						if(as.get(k)[1]<=l){
							continue;
						}
						if(WGen.gn(44100./(as.get(k)[0]-as.get(j)[0]))>nf+1){
							continue;
						}
						if(Math.abs(WGen.gn(44100./(as.get(k)[0]-as.get(j)[0]))-WGen.gn(44100./(as.get(j)[0]-as.get(i)[0])))<=.5){
							/*if(as.get(i)[0]==614097&&as.get(j)[0]==614930){
								Masa m=new Masa(2*44100./(as.get(j)[0]-as.get(i)[0]));
								double[]tmp=new double[as.get(k)[0]-as.get(i)[0]];
								for(int t=0;t<tmp.length;t++){
									tmp[t]=m.x;
									m.act(datos[t+as.get(i)[0]]);
								}
								Wave.p(tmp,nombre+"-c05.wav");
								System.exit(0);
							}*/
							Masa m=new Masa(2*44100./(as.get(j)[0]-as.get(i)[0]));
							m.act(datos,as.get(i)[0],as.get(k)[0]);
							if(m.o==8){
								c.U.imp(n+"---->"+as.get(i)[0]+"<>"+as.get(j)[0]+"<>"+as.get(k)[0]);
								int nv=(int)(((long)as.get(i)[1]+(long)as.get(j)[1]+(long)as.get(k)[1])/3);
								if(as.get(i)[0]>tfin){
									tini=as.get(i)[0];
									tfin=as.get(k)[0];
									val=nv;
									evs.add(new int[]{n,tini,tfin,val});
									continue;
								}
								if(as.get(i)[0]<=tfin){
									if(evs.get(evs.size()-1)[0]==n){
										if(evs.get(evs.size()-1)[3]<nv){
											evs.get(evs.size()-1)[3]=nv;
										}
										evs.get(evs.size()-1)[2]=as.get(k)[0];
										tfin=as.get(k)[0];
										if(nv>val){
											val=nv;
										}
										continue;
									}else{
										if(val<nv){
											evs.get(evs.size()-1)[2]=as.get(i)[0];
											tini=as.get(i)[0];
											tfin=as.get(k)[0];
											val=nv;
											evs.add(new int[]{n,tini,tfin,val});
										}
										continue;
									}
								}
							}
						}
					}
				}
			}
			Midiw m=new Midiw(nombre);
			m.sa=new int[0];
			for(int i=0;i<evs.size();i++){
				c.U.imp(evs.get(i)[0]+":"+evs.get(i)[1]+"<>"+evs.get(i)[2]+"->"+evs.get(i)[3]);
				m.add(evs.get(i)[0],evs.get(i)[3],evs.get(i)[1],evs.get(i)[2]);
			}
			m.write();
		}catch(Throwable t){
			c.U.imp(t);
		}
	}
}