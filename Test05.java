import java.util.*;
import java.io.*;
import java.awt.*;
import java.awt.image.*;
import javax.imageio.*;
import static c.U.imp;
public class Test05{
	public static void main(String[]args){
		try{
			Wave w=new Wave(new File("Test03.wav"));
			double[]datos=w.getms();
			double[]r=new double[datos.length];
			double dt=Math.pow(2,1./(12.*10))-1.;
			c.U.imp("dt:"+dt);
			int l=10000;
			double tol=0;
			ArrayList<Masa>masa=Masa.rangoValidacion(48,60,dt);
			for(int i=0;i<datos.length;i++){
				if(i%l==0&&i>0){
					for(int j=1;j<masa.size();j=j+3){
						if(masa.get(j).m>masa.get(j+1).m+tol&&masa.get(j).m>masa.get(j-1).m+tol){
							masa.get(j).res();
							for(int k=i-l;k<i;k++){
								masa.get(j).act(datos[k]);
								r[k]=r[k]+masa.get(j).x;
							}
						}
					}
					for(int j=0;j<masa.size();j++){
						masa.get(j).res();
					}
				}
				for(int j=0;j<masa.size();j++){
					masa.get(j).act(datos[i]);
				}
			}
			Wave.p(r,"resultado05.wav");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}