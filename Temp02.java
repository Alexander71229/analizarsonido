import java.util.*;
import java.io.*;
import java.awt.*;
import java.awt.image.*;
import javax.imageio.*;
public class Temp02{
	public static void main(String[]args){
		try{
			int ni=28;
			int nf=64;
			int dt=0;
			c.U.imp("Inicio");
			String nombre="D:\\Wavs\\S01";
			double[]datos=new Wave(new File(nombre+".wav")).getms();
			int tm=2000;
			double[][]xms=new double[nf-ni+1][datos.length/tm+1];
			ArrayList<int[]>as=WGen.amplitudes(datos);
			for(int i=0;i<as.size()-1;i++){
				if(as.get(i)[1]<=0){
					continue;
				}
				for(int j=i+2;j<as.size()&&WGen.gn(44100./(as.get(j)[0]-as.get(i)[0]))<=nf-1;j=j+2){
					int n=(int)Math.round(WGen.gn(44100./(as.get(j)[0]-as.get(i)[0])));
					if(n>=ni&&n<=nf){
						xms[n-ni][as.get(i)[0]/tm]+=Math.min(as.get(i)[1],-as.get(i+1)[1]);
					}
				}
			}
			ImageIO.write(SpecPrint.gen(xms,33000),"png",new File(nombre+"vT02.png"));
		}catch(Exception e){
			e.printStackTrace();
		}
	}
} 