import java.util.*;
import java.io.*;
public class Conv{
	double[]datos=null;
	public int lm;
	public int ni;
	public int nf;
	public int dt;
	public Conv(int ni,int nf,int dt,int lm){
		this.ni=ni;
		this.nf=nf;
		this.dt=dt;
		this.lm=lm;
	}
	public double[]test(){
		double[]resultado=new double[10000];
		for(int i=0;i<resultado.length;i++){
			resultado[i]=32000.*Math.sin(440.*i/44100.*2*Math.PI);
		}
		return resultado;
	}
	public void analisisEspectro(Midiw ev,ArrayList<Masa>d,int t,int k){
		int c=0;
		int dtn=2*dt+1;
		int e=0;
		int ei=-1;
		double evi=0;
		double mxm=0;
		int epm=-1;
		int eg=-1;
		int egi=-1;
		double egvi=0;
		int ipe=0;
		int iip=-1;
		double iivi=-1;
		double tpn=30; //Tolerancia pendiente
		double rgs=20; //Medición para el descenso
		int pdm=3; //Base para la medición de la pendiente
		double vvm=400; //Altura mínima requerida
		double vms=0.5; //Porcentaje para determinar la finalización
		int tmt=10; //Distancia mínima desde el máximo al fin.
		for(int i=0;i<d.size()-1;i++){
			if(ipe<=0){
				if(d.get(i+1).m-d.get(i).m>0){
					if(iip>=0&&iivi-d.get(i).m>rgs){
						eg=-1;
						egvi=iivi;
						egi=iip;
					}
					ipe=1;
					iip=i;
					iivi=d.get(i).m;
				}
			}else{
				if(d.get(i+1).m-d.get(i).m<=0){
					if((i-iip)>=pdm&&(d.get(i).m-d.get(i-pdm).m)>tpn){
						eg=1;
						egvi=iivi;
						egi=iip;
					}
					ipe=-1;
					iip=i;
					iivi=d.get(i).m;
				}
			}
			if(e<=0){
				if(eg>0){
					mxm=d.get(i).m;
					epm=i;
					evi=egvi;
					ei=egi;
					e=1;
				}else{
					e=-1;
				}
			}else{
				if(eg<=0&&((d.get(i).m-evi)/(mxm-evi)<vms||evi-d.get(i).m>=0||i-epm>tmt)){
					if(mxm-evi>vvm){
						//vis[epm]=['blue','blue'];
						int nt=epm/dtn+ni-1;
						//System.out.println(ev.nombre);
						//System.out.println("epm:"+epm);
						//System.out.println("nt:"+nt+" ti:"+(t+1-lm)+" tf:"+t);
						ev.add(nt,d.get(epm).m,t+1-lm,t);
						c++;
					}
					e=-1;
				}else{
					e=1;
				}
			}
			if(e>0){
				if(d.get(i).m>mxm){
					mxm=d.get(i).m;
					epm=i;
				}
			}
		}
	}
	public void pro()throws Exception{
		String nombre="Core Bride output0999j6";
		PrintStream ps=new PrintStream(new FileOutputStream(new File(nombre+".js")));
		int k=0;
		Wave w=new Wave(new File(nombre+".wav"));
		datos=w.getms();
		//datos=test();
		ArrayList<Masa>masas=Masa.rango(ni,nf,dt);
		int dtn=2*dt+1;
		String ini="ni="+ni+";\n";
		ini+="dt="+dt+";\n";
		ini+="lm="+lm+";\n";
		StringBuffer res=new StringBuffer();
		double max=0;
		Midiw ev=new Midiw(nombre);
		for(int t=0;t<datos.length;t++){
			for(int i=0;i<masas.size();i++){
				double d=datos[t];
				masas.get(i).act(d);
			}
			if(t%lm==lm-1){
				analisisEspectro(ev,masas,t,k);
				res.append("datos["+k+"]=[");
				String s="";
				double mx=0;
				int imx=0;
				for(int i=0;i<masas.size();i++){
					if(masas.get(i).m>max){
						max=masas.get(i).m;
					}
					//20141027:Se reemplazará por análisis de espectro
					/*if(i%dtn==0){
						if(i>0&&imx!=0&&imx!=2*dt){
							int nt=i/dtn+ni-1;
							ev.add(nt,mx,t+1-lm,t);
						}
						mx=0;
					}
					if(masas.get(i).m>mx){
						mx=masas.get(i).m;
						imx=i%dtn;
					}*/
					res.append(s+masas.get(i).m);
					masas.get(i).m=0;
					s=",";
				}
				res.append("];\n");
				k++;
			}
		}
		ini+="mx="+max+";\n";
		ps.println(ini+res);
		ev.write();
	}
}