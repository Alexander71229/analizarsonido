import java.util.*;
import java.io.*;
import java.awt.*;
import java.awt.image.*;
import javax.imageio.*;
import static c.U.imp;
public class T001{
	public static void main(String[]argumentos){
		try{
			c.U.ruta="log.txt";
			imp("Inicio");
			String nombre="T001";
			double[]d=new Wave(new File(nombre+".wav")).getms();
			int a=10;
			int b=200;
			int h=b-a;
			int w=2*b;
			double[][]xms=new double[h][w];
			double mx=0;
			for(int i=0;i<d.length;i++){
				for(int j=a;j<b;j++){
					int sp=j/2;
					if(i<w){
						xms[j-a][i]=Math.abs(d[i]+d[i+a]-d[i+sp]-d[i+sp+a]);
						if(xms[j-a][i]>mx){
							mx=xms[j-a][i];
						}
					}
				}
			}
			ImageIO.write(SpecPrint.gen(xms,mx),"png",new File(nombre+"_T001.png"));
			imp("Fin");
		}catch(Throwable t){
			imp(t);
		}
	}
}