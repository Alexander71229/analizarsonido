import javax.sound.sampled.*;
import java.io.*;
import java.nio.ShortBuffer;
import java.nio.ByteBuffer;
class Wave{
  AudioInputStream ais;
  AudioFormat af;
  double fr;
  int fs;
  int pos=0;
  public Wave(){
  }
  public Wave(File f) throws Exception{
	  setFile(f);
  }
  public double getFr(){
    return this.fr;
  }
  public void actAis(){
		this.af=this.ais.getFormat();
    this.fs=this.af.getFrameSize();
    this.fr=this.af.getFrameRate();
  }
  public void setAis(AudioInputStream ais){
    this.ais=ais;
    actAis();
  }
  public void setFile(File f) throws Exception{
		this.ais=AudioSystem.getAudioInputStream(f);
		actAis();
  }
	public int conv(byte[]bs){
		int MSB=0;
		int LSB=0;
		int respuesta=0;
		if(af.getSampleSizeInBits()==16)
		{
			if(af.isBigEndian())
			{
				MSB = (int)bs[0];
				LSB = (int)bs[1];
				respuesta = MSB<<8|(255&LSB);
			}
			else
			{
				LSB = (int)bs[0];
				MSB = (int)bs[1];
				respuesta = MSB<<8|(255&LSB);
			}
		}
		if (af.getSampleSizeInBits()==8)
		{
			if (af.getEncoding().toString().startsWith("PCM_SIGN"))
			{
				respuesta=bs[0];
			}
			else
			{
				respuesta = bs[0]-128;
			}
		}
		return respuesta;
	}
  public double getm() throws Exception{
		byte[]bs=new byte[2];
		double respuesta=0;
		if(af.getFrameSize()==2){
			ais.read(bs,0,2);
			//pos=pos+2;
		}
		if(af.getFrameSize()==1){
			ais.read(bs,0,1);
			//pos++;
		}
		respuesta=conv(bs);
	  return respuesta;
  }
	public double getm(int p) throws Exception{
		byte[]bs=new byte[2];
		double respuesta=0;
		if(af.getFrameSize()==2){
			ais.read(bs,p,2);
			//pos=pos+2;
		}
		if(af.getFrameSize()==1){
			ais.read(bs,p,1);
			//pos++;
		}
		respuesta=conv(bs);
		return respuesta;
	}
	public long getTamd(){
		if(ais!=null){
			return ais.getFrameLength();
		}
		return 0;
	}
	public static void p(byte[]ba,String name){
		try{
			ByteArrayInputStream is=new ByteArrayInputStream(ba);
			AudioFormat af=new AudioFormat((float)44100.,16,1,true,true);
			AudioInputStream ai=new AudioInputStream(is,af,ba.length/2);
			AudioSystem.write(ai,AudioFileFormat.Type.WAVE,new File(name));
		}catch(Exception e){
			System.out.println(e);
		}
	}	
	public static void p(double[]datos,String name){
		double max=0;
		byte[]ba=new byte[2*datos.length];
		ByteBuffer bb=ByteBuffer.wrap(ba);
		ShortBuffer sb=bb.asShortBuffer();
		for(int i=0;i<datos.length;i++){
			if(Math.abs(datos[i])>max){
				max=Math.abs(datos[i]);
			}
		}
		for(int i=0;i<datos.length;i++){
			short d=(short)(Math.round(32500*datos[i]/max));
			sb.put(d);
		}
		p(ba,name);
	}
  public double[]getms() throws Exception{
		int tmd=(int)this.getTamd();
		double[]respuesta=new double[tmd];
		byte[]bs=new byte[2];
		for(int i=0;i<tmd;i++){
			respuesta[i]=getm();
		}
	  return respuesta;
  }
	public double[]getms(int a,int b) throws Exception{
		//int tmd=(int)this.getTamd();
		int tmd=b-a;
		double[]respuesta=new double[tmd];
		byte[]bs=new byte[2];
		for(int i=0;i<tmd;i++){
			if(i==0){
			  //respuesta[i]=getm(a);
				ais.skip(a);
				respuesta[i]=getm();
			}else{
			  respuesta[i]=getm();
			}
		}
		return respuesta;
	}
}