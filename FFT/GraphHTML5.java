import java.io.*;
import java.util.*;
public class GraphHTML5{
	public String name;
	public int w;
	public int h;
	PrintStream ps;
	public StringBuffer codigo;
	public GraphHTML5(String name,int w,int h){
		this.w=w;
		this.h=h;
		this.name=name;
		File f=new File(this.name+".htm");
		try{
			ps=new PrintStream(new FileOutputStream(f));
		}catch(Exception e){
			e.printStackTrace();
		}
		codigo=new StringBuffer();
	}
	public GraphHTML5(int w,int h){
		this.w=w;
		this.h=h;
		codigo=new StringBuffer();
	}
	public void escribir(){
		ps.println("<body><canvas id='idCanvas' width='"+w+"' height='"+h+"'></canvas><script>");
		ps.println("ctx=document.getElementById('idCanvas').getContext('2d');");
		ps.println(codigo);
		ps.println("</script></body>");
	}
	public void linea(double px,double py,double px2,double py2,String color){
		int x=(int)Math.round(px);
		int y=(int)Math.round(py);
		int x2=(int)Math.round(px2);
		int y2=(int)Math.round(py2);
		codigo.append("ctx.strokeStyle='"+color+"';\n");
		codigo.append("ctx.moveTo("+x+","+y+");\n");
		codigo.append("ctx.lineTo("+x2+","+y2+");\n");
		codigo.append("ctx.stroke();\n");
	}
	public void iniciar(){
		codigo=new StringBuffer("");
	}
	public void fillRect(double px,double py,double pw,double ph,String color){
		int x=(int)Math.round(px);
		int y=(int)Math.round(py);
		int w=(int)Math.round(pw);
		int h=(int)Math.round(ph);
		codigo.append("ctx.fillStyle='"+color+"';\n");
		codigo.append("ctx.fillRect("+x+","+y+","+w+","+h+");\n");
	}
}