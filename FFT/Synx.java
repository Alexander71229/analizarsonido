import java.io.*;
public class Synx{
	public double[]anx(double[]d,int ia,int ib){
		int a=-1;
		int b=-1;
		double[]dx=new double[d.length];
		for(int t=0;t<d.length-1;t++){
			if((d[t]<=0.&&d[t+1]>0.)||(d[t]>=0.&&d[t+1]<0.)){
				if(a<0){
					a=t;
				}else{
					if(t-a>=ia&&t-a<=ib){
						b=t;
					}
				}
				if(a>=0&&b>0){
					//System.out.println(b-a);
					if(dft(d,a,b,dx)){
						//System.out.println("T:"+(b-a));
						a=b;
						b=-1;
					}else{
						//System.out.println("F:"+(b-a));
					}
				}
			}
		}
		return dx;
	}
	public double[]anx2(double[]d,int l){
		int a=-1;
		int b=-1;
		double[]dx=new double[d.length];
		for(int t=0;t<d.length-l;t=t+l){
			dft(d,t,t+l,dx);
		}
		return dx;
	}
	public boolean dft(double[]o,int a,int b,double[]d){
		int n=1;
		for(int j=a;j<b;j++){
			d[j]=0;
		}
		//for(int i=1;i<=n;i++){
		for(int i=2;(b-a)*1./i>3;i=i+2){
			double x=0;
			for(int j=a;j<b;j++){
				x+=o[j]*Math.sin(2*Math.PI*(j-a)*i/(b-a));
			}
			x=2*x/(b-a);
			for(int j=a;j<b;j++){
				d[j]+=x*Math.sin(2*Math.PI*(j-a)*i/(b-a));
			}
			x=0;
			for(int j=a;j<b;j++){
				x+=o[j]*Math.cos(2*Math.PI*(j-a)*i/(b-a));
			}
			x=2*x/(b-a);
			for(int j=a;j<b;j++){
				d[j]+=x*Math.cos(2*Math.PI*(j-a)*i/(b-a));
			}
		}
		//System.out.println("a:"+a+" b:"+b+" l:"+(b-a)+" oa:"+Math.round(o[a])+" ob:"+Math.round(o[b-1])+" da:"+Math.round(d[a])+" db:"+Math.round(d[b-1]));
		//if(Math.abs(d[a])<7000.&&Math.abs(d[b-1])<7000.){
		if(true){
		//if(Math.abs(d[a]-o[a])<7000.&&Math.abs(d[b-1]-d[b-1])<7000.){
			return true;
		}
		return false;
	}
	public void g(double[]d,String n){
		int w=1000,h=400;
		GraphHTML5 gx=new GraphHTML5(n,w,h);
		for(int i=0;i<1000;i++){
			gx.linea(i,(4-d[i]/32000.)*h/8.,i+1,(4-d[i+1]/32000.)*h/8.,"blue");
		}
		gx.linea(0,(4-0)*h/8.,w,(4-0)*h/8.,"red");
		gx.escribir();
	}
	public static void main(String[]args){
		try{
			int l=44100;
			double[]d=new double[l];
			/*for(int t=0;t<l;t++){
				//d[t]=32000.*Math.sin(2.*Math.PI*t/100.)+20000.*Math.sin(2.*2.5*Math.PI*t/100.);
				//d[t]=32000.*Math.sin(2.*Math.PI*t/100.)+32000.*Math.sin(2.*2.5*Math.PI*t/100.)+32000.*Math.sin(2.*1.5*Math.PI*t/100.);
				d[t]=32000.*Math.sin(2.*Math.PI*t/100.)+32000.*Math.sin(2.*2.5*Math.PI*t/100.)+32000.*Math.sin(2.*1.5*Math.PI*t/100.)+32000.*Math.cos(2.*4.3*Math.PI*t/100.);
				//d[t]=32000.*Math.sin(2.*Math.PI*t/100.);
			}*/
			Synx s=new Synx();
			//s.g(d,"in");
			//s.g(s.anx(d,500,10000),"ou");
			Wave w=new Wave(new File("Test3.wav"));
			d=w.getms();
			Wave.p(s.anx(d,109,170),"Out.wav");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}