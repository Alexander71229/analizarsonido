import java.io.File;
import java.util.ArrayList;
public class Test02{
	public static void test01()throws Exception{
		ArrayList<Integer>periodos=new ArrayList<Integer>();
		ArrayList<ArrayList<Double>>amplitudes=new ArrayList<ArrayList<Double>>();
		periodos.add(100);
		amplitudes.add(new ArrayList<Double>());
		amplitudes.get(0).add(1000.);
		amplitudes.get(0).add(1500.);
		amplitudes.get(0).add(3500.);
		amplitudes.get(0).add(13500.);
		amplitudes.get(0).add(23500.);
		amplitudes.get(0).add(30500.);
		amplitudes.get(0).add(13500.);
		Wave.p(WGen.generarDesdeEspectro(periodos,amplitudes),"resultado01.wav");
	}
	public static void main(String[]argumentos){
		try{
			int p=100;
			ArrayList<Integer>periodos=new ArrayList<Integer>();
			periodos.add(p);
			ArrayList<ArrayList<Double>>amplitudes=new ArrayList<ArrayList<Double>>();
			amplitudes.add(new ArrayList<Double>());
			//double[]datos=(new Wave(new File("input01.wav"))).getms();
			//double[]datos=WGen.cuadrada01(12000,50,44100);
			//double[]datos=WGen.simple(12000,50,44100);
			double[]datos=WGen.concatenar(WGen.cuadrada01(12000,50,44100),WGen.concatenar(WGen.silencio(44100),WGen.cuadrada01(12000,50,44100)));
			double tot=0;
			double atot=0;
			double[]resultado=new double[datos.length];
			int desfase=11000;
			for(int t=0;t<datos.length;t++){
				tot=tot+Math.sin(2.*Math.PI*t/p)*datos[t];
				if(t>=desfase){
					atot=atot+Math.sin(2.*Math.PI*(t-desfase)/p)*datos[(t-desfase)];
				}
				if(t%p==0&&t>0){
					amplitudes.get(0).add(2.*tot/p);
					//tot=0;
				}
				//resultado[t]=tot/(t+1)*Math.sin(2.*Math.PI*t/p);
				//resultado[t]=tot/(t+1);
				//resultado[t]=(tot-atot)/desfase;
				resultado[t]=atot;
			}
			periodos=new ArrayList<Integer>();
			amplitudes=new ArrayList<ArrayList<Double>>();
			/*for(int i=90;i<=110;i++){
				periodos.add(i);
			}*/
			//periodos.add(50);
			periodos.add(100);
			for(int i=0;i<periodos.size();i++){
				amplitudes.add(new ArrayList<Double>());
			}
			//resultado=WGen.generarDesdeEspectro(periodos,Fourier.sinusoidePendiente(datos,periodos));
			//Wave.p(resultado,"resultado01.wav");
			//Wave.p(WGen.integral(WGen.derivada((datos))),"resultado01.wav");
			//Wave.p(WGen.sumar(datos,1,WGen.derivada(datos),-103+*Math.PI/2,0),"resultado01.wav");
			double[]a=WGen.integral(WGen.convolucion(datos,100,0));
			double[]b=WGen.integral(WGen.convolucion(datos,100,0));
			Wave.p(WGen.sumar(a,1,b,-1,1),"resultado01.wav");
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
}