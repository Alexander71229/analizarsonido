import java.util.*;
import javax.sound.midi.*;
import java.io.*;
class Ev{
	public int n;
	public int v;
	public double ti;
	public double tf;
	public Ev(){
	}
	public Ev(int n,int v,double ti,double tf){
		this.n=n;
		this.v=v;
		this.ti=ti;
		this.tf=tf;
	}
	public String toString(){
		return this.n+","+this.v+","+this.ti+","+this.tf;
	}
}
public class Midin{
	public ArrayList<Ev>evs=null;
	public HashMap<Integer,Ev>h;
	public HashMap<Integer,Integer>ha;
	public double tm;
	public Midin(int tm){
		evs=new ArrayList<Ev>();
		h=new HashMap<Integer,Ev>();
		this.tm=tm;
	}
	public void add(int n,int v,int ti,int tf){
		//c.U.imp("add:"+n+":"+ti+"<>"+tf);
		if(evs.size()==0){
			evs.add(new Ev(n,v,ti,tf));
		}else{
			if(evs.get(evs.size()-1).n==n&&evs.get(evs.size()-1).tf==ti){
				evs.get(evs.size()-1).tf=tf;
			}else{
				evs.add(new Ev(n,v,ti,tf));
			}
		}
	}
	public void write(String nombre,double len) throws Exception{
		Sequence sq=new Sequence(Sequence.PPQ,384,2);
		for(int i=0;i<evs.size();i++){
			Ev ev=evs.get(i);
			if(ev.tf-ev.ti>len&&ev.n<137){
				ShortMessage sm=new ShortMessage();
				sm.setMessage(0x90,ev.n,ev.v);
				long pos=Math.round(ev.ti*(768./tm));
				MidiEvent me=new MidiEvent(sm,pos);
				sq.getTracks()[1].add(me);
				sm=new ShortMessage();
				sm.setMessage(0x80,ev.n,0);
				pos=Math.round(ev.tf*(768./tm));
				me=new MidiEvent(sm,pos);
				sq.getTracks()[1].add(me);
			}
		}
		if(sq.getTracks()[1].size()>0){
			MidiSystem.write(sq,1,new File(nombre+".mid"));
		}
	}
}