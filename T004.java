import java.util.*;
import java.io.*;
import javax.sound.midi.*;
import static c.U.imp;
class Mi{
	public ArrayList<Integer>n=new ArrayList<>();
	public ArrayList<Long>a=new ArrayList<>();
	public ArrayList<Long>b=new ArrayList<>();
	public long mx;
}
public class T004{
	public static Mi secuencia(Sequence s,int c){
		MTempo mts=new MTempo(s);
		ArrayList<Integer>n=new ArrayList<>();
		ArrayList<Long>a=new ArrayList<>();
		ArrayList<Long>b=new ArrayList<>();
		HashMap<Integer,Long>h=new HashMap<>();
		long mx=0;
		for(int i=0;i<s.getTracks().length;i++){
			Track t=s.getTracks()[i];
			for(int j=0;j<t.size();j++){
				MidiEvent me=t.get(j);
				if(me.getMessage()instanceof ShortMessage){
					ShortMessage sm=(ShortMessage)me.getMessage();
					if(sm.getChannel()==c){
						if(sm.getCommand()==0x90&&sm.getData2()>0){
							h.put(sm.getData1(),mts.tiempo(me.getTick()));
							continue;
						}
						if((sm.getCommand()==0x90&&sm.getData2()==0)||(sm.getCommand()==0x80)){
							long x=mts.tiempo(me.getTick());
							n.add(sm.getData1());
							a.add(h.get(sm.getData1()));
							b.add(x);
							//imp(h.get(sm.getData1())+":"+sm.getData1()+":"+x);
							h.remove(sm.getData1());
							if(x>mx){
								mx=x;
							}
							continue;
						}
					}
				}
			}
		}
		Mi r=new Mi();
		r.a=a;
		r.b=b;
		r.n=n;
		r.mx=mx;
		return r;
	}
	public static void seg(double[]r,int n,long a,long b,long[]q,double[]datos,ArrayList<Masa>m){
		n=n-12;
		int ia=(int)Math.round(a*441./10000.);
		int ib=(int)Math.round(b*441./10000.);
		int[]x=new int[m.size()];
		ArrayList<ArrayList<Double>>d=new ArrayList<>();
		ArrayList<ArrayList<Integer>>s=new ArrayList<>();
		ArrayList<Masa>mz=Masa.rango(n,n,0,30);
		for(int i=0;i<m.size();i++){
			m.get(i).w=mz.get(i).w;
			m.get(i).f=mz.get(i).f;
			m.get(i).m=0;
			m.get(i).cx=.995;
		}
		for(int i=0;i<x.length;i++){
			x[i]=1;
			d.add(new ArrayList<>());
			s.add(new ArrayList<>());
		}
		long z=q[1]+ib-ia-(q[2]-q[0]);
		int k=(int)q[0];
		for(long t=ia;t<ib;t++){
			if(k>=datos.length){
				break;
			}
			double v=datos[k];
			if(t-ia>=q[1]-q[0]&&t-ia<z-q[0]){
				v=0;
			}else{
				k++;
			}
			if(t-ia==q[1]-q[0]){
				for(int i=0;i<m.size();i++){
					m.get(i).cx=1.;
				}
			}
			if(t-ia>=z-q[0]){
				for(int i=0;i<m.size();i++){
					m.get(i).cx=.995;
				}
			}
			for(int i=0;i<m.size();i++){
				m.get(i).act(v);
				if(t>=(x[i]*44100./m.get(i).f+ia)){
					/*if(i==0){
						d.get(i).add(3000.*m.get(i).m);
					}else{
						d.get(i).add(300.*m.get(i).m);
					}*/
					d.get(i).add(3000.*m.get(i).m/(m.get(i).f*m.get(i).f));
					//d.get(i).add(300.*m.get(i).m);
					s.get(i).add((int)t);
					x[i]++;
					m.get(i).m=0;
				}
			}
		}
		for(int i=0;i<d.size();i++){
			d.get(i).add(0.);
			s.get(i).add(2*s.get(i).get(s.get(i).size()-1)-s.get(i).get(s.get(i).size()-2));
		}
		for(int i=0;i<d.size();i++){
			d.get(i).add(0,0.);
			s.get(i).add(0,2*s.get(i).get(0)-s.get(i).get(1));
		}
		for(int i=0;i<d.size();i++){
			WGen.interpolacion(r,d.get(i),s.get(i),m.get(i).f);
		}
		WGen.normalizar(r,s.get(0).get(0),s.get(0).get(s.get(0).size()-1),30000);
	}
	public static double[]pro(Scanner sc,Mi mi,double[]datos,ArrayList<Masa>m){
		double[]r=new double[(int)Math.ceil(mi.mx*441./10000.)];
		for(int i=0;i<mi.n.size();i++){
			if(!sc.hasNextLine()){
				break;
			}
			String ln=sc.nextLine();
			if(ln.length()==0){
				break;
			}
			String[]l=ln.replaceAll("\\.","").split(" ");
			long[]q=new long[3];
			for(int j=0;j<q.length;j++){
				q[j]=Long.parseLong(l[j]);
			}
			seg(r,mi.n.get(i),mi.a.get(i),mi.b.get(i),q,datos,m);
		}
		return r;
	}
	public static void main(String[]argumentos){
		try{
			c.U.ruta="log.txt";
			imp("Inicio");
			String nombre="G:\\Java\\Txtmid\\txtmid\\Cumple.vs";
			Scanner sc=new Scanner(new File(nombre));
			Sequence s=MidiSystem.getSequence(new File(sc.nextLine()));
			double[]datos=new Wave(new File(sc.nextLine())).getms();
			Masa.coe=0.995;
			double[]r=pro(sc,secuencia(s,1),datos,Masa.rango(60,60,0,30));
			Wave.p(r,nombre+"-T004.wav");
			imp("Fin");
		}catch(Throwable t){
			imp(t);
		}
	}
}