import java.util.*;
public class O{
	public double p;
	public static double tm=44100.;
	public Masa m;
	public double mx;
	public O(double p){
		this.p=p;
		this.m=new Masa(O.tm/this.p);
		//this.m=new Masa(O.tm/4.);
		this.m.tmm=O.tm;
	}
	public double act(double[]d,int a,int b){
		double r=0;
		double f=0;
		//int h=0;
		for(int j=1;j<=1;j++){
			for(int z=0;z<=0;z++){
				f=z*(p/4.);
				for(int i=(int)Math.floor((a-f)/p),k=g(p,f,i);k<b;i++,k=g(p,f,i)){
					if(a<=k){//h++;
						r+=d[k];
					}
				}
				f=z*(p/4.)+p/2.;
				for(int i=(int)Math.floor((a-f)/p),k=g(p,f,i);k<b;i++,k=g(p,f,i)){
					if(a<=k){//h++;
						r-=d[k];
					}
				}
			}
		}
		//return r/Math.floor((b-a)/p);
		//return r/h;
		return r*p/(b-a);
	}
	public double act4(double[]d,int a,int b){
		double r=0;
		for(int j=0;j<this.p;j++){
			double z=0;
			int c=0;
			for(int i=a+j,k=1;i<b-this.p&&i<d.length&&((int)Math.round(a+j+k*this.p/2.))<d.length;i=(int)Math.round(a+j+k*this.p),k++){
				z+=(d[i]-d[(int)Math.round(a+j+k*this.p/2.)]);
				c++;
			}
			//z=Math.abs(z)/c;
			z=Math.abs(z);
			if(z>r){
				r=z;
			}
		}
		//return r*p/(b-a);
		this.mx=r;
		return r;
	}
	public double act8(double[]d,int a,int b){
		double r=0;
		int k=(int)Math.floor(a/this.p);
		int h=0;
		for(;((int)Math.round((k+.5)*this.p))>=a&&((int)Math.round((k+1)*this.p))<b&&((int)Math.round((k+1)*this.p))<d.length;k++){
			r+=(2*d[(int)Math.round((k+.5)*this.p)]-d[(int)Math.round((k+1)*this.p)]);
			h++;
		}
		this.mx=Math.abs(r)/h;
		return this.mx;
	}
	public double act5(double[]d,int a,int b){
		double r=0;
		int fm=0;
		for(int j=0;j<this.p;j++){
			double z=0;
			int c=0;
			for(int i=a+j,k=1;i<b-this.p&&i<d.length&&((int)Math.round(a+j+k*this.p/2.))<d.length;i=(int)Math.round(a+j+k*this.p),k++){
				z+=(d[i]-d[(int)Math.round(a+j+k*this.p/2.)]);
				c++;
			}
			//z=Math.abs(z)/c;
			z=Math.abs(z);
			if(z>r){
				fm=j;
				r=z;
			}
		}
		r=asf(d,a,b,(int)(fm+this.p/4.));
		this.mx=r;
		return r;
	}
	public double act2(double[]d,int a,int b){
		double f=0;
		this.m.res();
		for(int i=(int)Math.floor((a-f)/p),k=g(p,f,i);k<b;i++,k=g(p,f,i)){
			this.m.act(d[k]);
		}
		return this.m.m;
	}
	public double act7(double[]d,int a,int b){
		this.mx=this.m.act(d,a,b)*this.p;
		return this.mx;
	}
	public double act3(double[]d,int a,int b){
		double r=0;
		int f=1;
		for(int j=0;j<this.p;j++){
			double z=0;
			for(int t=a;t<b&&t<d.length;t=t+f){
				z+=Math.sin(f*2.*Math.PI*(t+j)/this.p)*d[t];
				//r+=Math.cos(f*2.*Math.PI*(t+j)/this.p)*d[t];
			}
			z=Math.abs(z);
			if(z>r){
				r=z;
			}
		}
		this.mx=r*p/(b-a);
		return this.mx;
	}
	public double act6(double[]d,int a,int b){
		double r=0;
		int f=1;
		for(int j=0;j<=0;j++){
			double z=0;
			for(int t=a;t<b&&t<d.length;t=t+f){
				z+=Math.sin(f*2.*Math.PI*(t+j)/this.p)*d[t];
			}
			z=Math.abs(z);
			if(z>r){
				r=z;
			}
		}
		//this.mx=r*p/(b-a);
		this.mx=r;
		return this.mx;
	}
	public double asf(double[]d,int a,int b,int j){
		double r=0;
		int f=1;
		double z=0;
		for(int t=a;t<b&&t<d.length;t=t+f){
			z+=Math.sin(f*2.*Math.PI*(t+j)/this.p)*d[t];
		}
		z=Math.abs(z);
		if(z>r){
			r=z;
		}
		//return r*p/(b-a);
		return r;
	}
	public static int g(double p,double f,int i){
		return(int)Math.round(p*i+f);
	}
	public static ArrayList<O>rango(int ni,int nf,int dt){
		int tc=dt*2+1;
		ArrayList<O>resultado=new ArrayList<>(tc);
		for(int n=ni;n<=nf;n++){
			for(int j=-dt;j<=dt;j++){
				resultado.add(new O(O.tm/(440.*Math.pow(2.,((n-69.)+j/(tc*1.))/12.))));
			}
		}
		return resultado;
	}
	public static ArrayList<O>rango2(double pi,double pf,double dt){
		ArrayList<O>resultado=new ArrayList<>();
		for(double n=pi;n<=pf;n+=dt){
			resultado.add(new O(n));
		}
		return resultado;
	}
}