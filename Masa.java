import java.util.*;
public class Masa{
	public int n;
	public int o;
	public double c;
	public double f;
	public double w;
	public double x;
	public double x1;
	public double x2;
	public double ax1;
	public double ax2;
	public double v;
	public double v1;
	public double v2;
	public double m;
	public double tmm;
	public static double coe=0.999;
	public static double tm=44100.;
	public double cx=0;
	public Masa(){
		c=1.;
		this.tmm=Masa.tm;
		this.cx=Masa.coe;
	}
	public Masa(double f){
		c=1.;
		setF(f);
		this.tmm=Masa.tm;
		this.cx=Masa.coe;
	}
	public Masa(int n,double f){
		c=1.;
		setF(f);
		this.n=n;
		this.tmm=Masa.tm;
		this.cx=Masa.coe;
	}
	public void setF(double f){
		if(f==0){
			f=30;
		}
		this.f=f;
		this.w=4*Math.PI*Math.PI*this.f*this.f;
		this.n=(int)Math.round(WGen.gn(this.f));
	}
	public void res(){
		this.x=0;
		this.v=0;
		this.m=0;
	}
	public void act(double d){
		x2=x1;
		x1=x;
		v2=v1;
		v1=v;
		v=(d-x)*w/this.tmm+v;
		x=(v/this.tmm+x)*cx;
		if(m<Math.abs(x))m=Math.abs(x);
		if(ax2<ax1&&ax1>x){
			o++;
		}
		if(ax2>ax1&&ax1<x){
			o++;
		}
		ax2=ax1;
		ax1=x;
	}
	public double act(double[]d,int a,int b){
		this.res();
		for(int j=1;j<=1;j++){
			for(int t=a;t<b&&t<d.length;t++){
				this.act(d[t]);
			}
		}
		return this.m;
	}
	public static ArrayList<Masa>rango(int ni,int nf,int dt){
		int tc=dt*2+1;
		ArrayList<Masa>resultado=new ArrayList<Masa>(tc);
		for(int n=ni;n<=nf;n++){
			for(int j=-dt;j<=dt;j++){
				resultado.add(new Masa(n,440*Math.pow(2.,((n-69.)+j/(tc*1.))/12.)));
				//System.out.println(resultado.get(resultado.size()-1).f);
			}
		}
		return resultado;
	}
	public static ArrayList<Masa>rango2(int ni,int nf,int dt){
		int tc=dt*2+1;
		ArrayList<Masa>resultado=new ArrayList<Masa>(tc);
		for(int n=ni;n<=nf;n++){
			for(int j=-dt;j<=dt;j++){
				Masa m=new Masa(n,440*Math.pow(2.,((nf-69.)+dt/(tc*1.))/12.));
				m.tmm=Masa.tm*m.f/(440*Math.pow(2.,((n-69.)+j/(tc*1.))/12.));
				resultado.add(m);
			}
		}
		return resultado;
	}
	//Armónicos
	public static ArrayList<Masa>rango(int ni,int nf,int dt,int na){
		int tc=dt*2+1;
		ArrayList<Masa>resultado=new ArrayList<Masa>(tc*na);
		for(int n=ni;n<=nf;n++){
			for(int j=-dt;j<=dt;j++){
				for(int i=1;i<=na;i++){
					resultado.add(new Masa(440*i*Math.pow(2.,((n-69.)+j/(tc*1.))/12.)));
				}
			}
		}
		return resultado;
	}
	//Validación
	public static ArrayList<Masa>rangoValidacion(int ni,int nf,double dt){
		ArrayList<Masa>resultado=new ArrayList<Masa>((nf-ni+1)*3);
		for(int n=ni;n<=nf;n++){
			double f=440*Math.pow(2.,(n-69.)/12.);
			resultado.add(new Masa(f/(1+dt)));
			resultado.add(new Masa(n,f));
			resultado.add(new Masa(f*(1+dt)));
		}
		return resultado;
	}
	public double energia(){
		return Math.sqrt(this.x*this.x+this.v*this.v);
	}
	public static double[]g(double[]datos,ArrayList<Masa>ms,int a,int b){
		double[]r=new double[b-a];
		for(int t=0;t<r.length;t++){
			double tx=0;
			for(int i=0;i<ms.size();i++){
				ms.get(i).act(datos[t+a]);
				tx+=ms.get(i).x*ms.get(i).f;
			}
			r[t]=tx;
		}
		return r;
	}
	public static void g(double[]datos,double[]salida,ArrayList<Masa>ms,int a,int b){
		for(int t=0;t<b-a;t++){
			double tx=0;
			for(int i=0;i<ms.size();i++){
				//tx+=ms.get(i).x*ms.get(i).f;
				tx+=ms.get(i).x;
				ms.get(i).act(datos[t+a]);
			}
			salida[t+a]=salida[t+a]+tx;
		}
	}
	public static void z(double[]salida,ArrayList<Masa>ms,int a,int b){
		for(int t=0;t<b-a;t++){
			double tx=0;
			for(int i=0;i<ms.size();i++){
				ms.get(i).act(0);
				tx+=ms.get(i).x*ms.get(i).f;
			}
			salida[t+a]=salida[t+a]+tx;
		}
	}
	public static double g(ArrayList<Masa>ms,double v){
		double r=0;
		for(int i=0;i<ms.size();i++){
			ms.get(i).act(v);
			r+=ms.get(i).x;
		}
		return r;
	}
	public static void resx(ArrayList<Masa>ms){
		for(int i=0;i<ms.size();i++){
			ms.get(i).res();
		}
	}
	public static int max(ArrayList<Masa>ms){
		double zm=0;
		int r=0;
		for(int i=0;i<ms.size();i++){
			if(ms.get(i).m>zm){
				r=i;
				zm=ms.get(i).m;
			}
		}
		return r;
	}
}