import java.util.*;
import java.io.*;
import java.awt.*;
import java.awt.image.*;
import javax.imageio.*;
public class Test07{
	public static void main(String[]args){
		try{
			String nombre="CabezaMusicalMusicalMusical";
			Wave w=new Wave(new File(nombre+".wav"));
			Midiw m=new Midiw(nombre);
			double[]datos=w.getms();
			ArrayList<Masa>masa=Masa.rango(48-24,72+12,0);
			//Masa.coe=0.995;
			//Masa.coe=1;
			int l=5000;
			double tol=0;
			for(int i=0;i<datos.length;i++){
				if(i%l==0&&i>0){
					for(int j=1;j<masa.size()-1;j++){
						if(masa.get(j).m>masa.get(j+1).m+tol&&masa.get(j).m>masa.get(j-1).m+tol){
							m.ini(masa.get(j).n,masa.get(j).m,i-l);
						}else{
							m.fin(masa.get(j).n,i-l);
						}
					}
					for(int j=0;j<masa.size();j++){
						masa.get(j).res();
					}
				}
				for(int j=0;j<masa.size();j++){
					masa.get(j).act(datos[i]);
				}
			}
			m.fin(datos.length);
			m.write(20,l+20);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}