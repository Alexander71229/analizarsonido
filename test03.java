import java.util.*;
import java.io.*;
import java.awt.*;
import java.awt.image.*;
import javax.imageio.*;
public class Test03{
	public static void main(String[]args){
		try{
			Wave w=new Wave(new File("Test03.wav"));
			double[]datos=w.getms();
			double dt=0.006;
			ArrayList<Masa>masa=Masa.rangoValidacion(48,60,dt);
			for(int i=0;i<datos.length;i++){
				double t=0;
				for(int j=0;j<masa.size();j++){
					masa.get(j).act(datos[i]);
				}
				for(int j=1;j<masa.size();j=j+3){
					if(masa.get(j).m>masa.get(j+1).m&&masa.get(j).m>masa.get(j-1).m){
						t+=masa.get(j).x;
					}
				}
				datos[i]=t;
			}
			Wave.p(datos,"resultado03.wav");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}