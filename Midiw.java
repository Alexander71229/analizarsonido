import java.util.*;
import javax.sound.midi.*;
import java.io.*;
class Ev{
	public int n;
	public double v;
	public double ti;
	public double tf;
	public Ev(){
	}
	public Ev(int n,double v,double ti,double tf){
		this.n=n;
		this.v=v;
		this.ti=ti;
		this.tf=tf;
	}
	public String toString(){
		return this.n+","+this.v+","+this.ti+","+this.tf;
	}
}
public class Midiw{
	public ArrayList<Ev>evs=null;
	public HashMap<Integer,Ev>h;
	public HashMap<Integer,Integer>ha;
	public String nombre;
	public double tm;
	public double m=0;
	public int[]sa={12,19,24,28,31};
	public Midiw(String nombre){
		this.nombre=nombre;
		evs=new ArrayList<Ev>();
		h=new HashMap<Integer,Ev>();
		ha=new HashMap<Integer,Integer>();
		tm=1;
		m=0.;
	}
	public void add(int n,double v,double ti,double tf){
		if(v>m){
			m=v;
		}
		evs.add(new Ev(n,v,ti/tm,tf/tm));
		ams(n);
	}
	public void ams(int n){
		for(int i=0;i<sa.length;i++){
			ha.put(n+sa[i],1);
		}
	}
	public void rms(int n){
		for(int i=0;i<sa.length;i++){
			ha.remove(n+sa[i]);
		}
	}
	public void ini(int n,double v,double t){
		if(n==0)return;
		if(h.get(n)==null){
			if(ha.get(n)==null){
				add(n,v,t,0);
				h.put(n,evs.get(evs.size()-1));
			}
		}else{
			if(v>h.get(n).v){
				h.get(n).v=v;
			}
		}
	}
	public void fin(int n,double t){
		if(h.get(n)!=null&&n>0){
			h.get(n).tf=t;
			h.remove(n);
			rms(n);
		}
	}
	public void fin(double t){
		h.values().forEach(e->e.tf=t);
	}
	public void write(int tol) throws Exception{
		write(tol,0);
	}
	public void write() throws Exception{
		write(20,0);
	}
	public void write(int tol,double len) throws Exception{
		Sequence sq=new Sequence(Sequence.PPQ,384,2);
		for(int i=0;i<evs.size();i++){
			Ev ev=evs.get(i);
			if(ev.tf>ev.ti+len){
				double v=ev.v;
				int vnt=(int)((Math.log(v*0.05+1)/Math.log(this.m*0.05+1))*127.);
				//int vnt=(int)(127.*v/this.m);
				//System.out.println(v+":"+vnt);
				if(vnt>127){
					vnt=127;
				}
				if(vnt>tol){
					c.U.imp("n:"+ev.n);
					ShortMessage sm=new ShortMessage();
					sm.setMessage(0x90,ev.n,vnt);
					long pos=(long)(ev.ti*(384./22050.));
					MidiEvent me=new MidiEvent(sm,pos);
					sq.getTracks()[1].add(me);
					sm=new ShortMessage();
					sm.setMessage(0x80,ev.n,0);
					pos=(long)(ev.tf*(384./22050.));
					me=new MidiEvent(sm,pos);
					sq.getTracks()[1].add(me);
				}
			}
		}
		if(sq.getTracks()[1].size()>0){
			MidiSystem.write(sq,1,new File(nombre+".mid"));
		}
	}
	public static void main(String[]args){
		try{
			Midiw m=new Midiw("Midiw");
			m.add(69,100,5000,10000);
			m.add(60,100,10000,20000);
			m.add(64,100,20000,30000);
			m.add(45,100,30000,40000);
			m.add(50,100,40000,50000);
			m.write();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}