import java.util.*;
import java.io.*;
import java.awt.*;
import java.awt.image.*;
import javax.imageio.*;
public class Test08{
	public static void main(String[]args){
		try{
			Masa.coe=1;
			int a=10;
			int b=1630;
			ArrayList<Masa>m=new ArrayList<>();
			for(int i=a;i<=b;i++){
				m.add(new Masa(44100./i));
			}
			{
				for(int i=0,j=a;i<m.size();i++,j++){
					double[]datos=WGen.sinusoide(1,j,j);
					for(int t=0;t<j&&t<datos.length;t++){
						m.get(i).act(datos[t]);
					}
					m.get(i).c=m.get(i).m;
					//c.U.imp(j+":"+m.get(i).c);
				}
			}
			for(int i=0;i<m.size();i++){
				m.get(i).res();
			}
			//double[]datos=WGen.sinusoide(32000,100,100);
			double[]datos=WGen.sumar(WGen.sinusoide(32000,100,100),1,WGen.sinusoide(32000,800,800),1,0);
			for(int i=0,j=a;i<m.size();i++,j++){
				for(int k=0;k<100;k++){
					for(int t=0;t<j&&t<datos.length;t++){
						m.get(i).act(datos[t]);
					}
				}
			}
			double mx=0;
			double mn=10000000.;
			for(int i=0,j=a;i<m.size();i++,j++){
				//m.get(i).m=m.get(i).m/m.get(i).c;
				if(m.get(i).m>mx){
					mx=m.get(i).m;
				}
				if(m.get(i).m<mn){
					mn=m.get(i).m;
				}
			}
			for(int i=1;i<m.size()-1;i++){
				if(m.get(i).m>m.get(i-1).m&&m.get(i).m>m.get(i+1).m){
					c.U.imp(m.get(i).f+":"+m.get(i).m+"->"+44100./m.get(i).f+"--->"+i);
				}
			}
			double[][]xms=new double[m.size()][1000];
			for(int i=0;i<m.size();i++){
				for(int j=0;j<1000;j++){
					xms[m.size()-i-1][j]=m.get(i).m-mn;
					//xms[i][j]=m.get(i).m-mn;
				}
			}
			ImageIO.write(SpecPrint.gen(xms,mx-mn),"png",new File("Test08.png"));
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}