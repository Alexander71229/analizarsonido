import java.util.*;
import java.io.*;
import java.awt.*;
import java.awt.image.*;
import javax.imageio.*;
public class Test12{
	public static void main(String[]args){
		try{
			String nombre="T12";
			int p=800;
			int d=50;
			double[]datos=WGen.sinusoide(1,p,(p+d)*2);
			int pi=p-d;
			int pf=p+d;
			int dt=1;
			ArrayList<O>m=O.rango2(pi,pf,dt);
			int tm=datos.length-1;
			int lx=100;
			int l=datos.length;
			double[][]xms=new double[m.size()][lx*l/tm];
			for(int t=tm;t<l&&t<datos.length;t+=tm){
				for(int i=0;i<m.size();i++){
					double v=m.get(i).act4(datos,t-tm,t);
					for(int k=0;k<lx;k++){
						xms[i][(t/tm-1)*lx+k]=v;
					}
				}
				if(t%(tm*10)==0){
					System.out.println(Math.round((100*t)/datos.length));
				}
			}
			ImageIO.write(SpecPrint.gen(xms,SpecPrint.gmx(xms)),"png",new File(nombre+"v12.png"));
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}