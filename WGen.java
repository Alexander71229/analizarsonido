import java.util.ArrayList;
public class WGen{
	public static double[]cuadrada01(double amplitud,int semiPeriodo,int tam){
		double[]resultado=new double[tam];
		for(int t=0;t<tam;t++){
			int p=t%(semiPeriodo*2);
			if(p<semiPeriodo){
				resultado[t]=amplitud;
			}else{
				resultado[t]=0;
			}
		}
		return resultado;
	}
	public static double[]simple(double amplitud,int semiPeriodo,int tam){
		double[]resultado=new double[tam];
		for(int t=0;t<tam;t++){
			resultado[t]=amplitud*Math.sin(Math.PI*t/semiPeriodo);
		}
		return resultado;
	}
	public static double[]sinusoide(double amplitud,double periodo,int tam){
		double[]resultado=new double[tam];
		for(int t=0;t<tam;t++){
			resultado[t]=amplitud*Math.sin(2*Math.PI*t/periodo);
		}
		return resultado;
	}
	public static double[]sinusoide(double amplitud,double periodo,double fase,int tam){
		double[]resultado=new double[tam];
		for(int t=0;t<tam;t++){
			resultado[t]=amplitud*Math.sin(2*Math.PI*(t+fase)/periodo);
		}
		return resultado;
	}
	public static double[]generarDesdeEspectro(ArrayList<Integer>periodos,ArrayList<ArrayList<Double>>amplitudes){
		int max=0;
		for(int i=0;i<periodos.size();i++){
			int duracion=periodos.get(i)*amplitudes.get(i).size();
			if(max<duracion){
				max=duracion;
			}
		}
		double[]resultado=new double[max];
		int[]indices=new int[amplitudes.size()];
		for(int t=0;t<max;t++){
			double tot=0;
			for(int i=0;i<periodos.size();i++){
				if((indices[i]+1)*periodos.get(i)<=t){
					indices[i]++;
				}
				if(indices[i]<amplitudes.get(i).size()){
					double amplitud=0;
					double anterior=0;
					if(indices[i]>0){
						anterior=amplitudes.get(i).get(indices[i]-1);
					}
					amplitud=(amplitudes.get(i).get(indices[i])-anterior)*((t%periodos.get(i))*1./periodos.get(i))+anterior;
					tot+=amplitud*Math.sin(2.*Math.PI*t/periodos.get(i));
				}
			}
			resultado[t]=tot;
		}
		return resultado;
	}
	public static double[]silencio(int tam){
		double[]resultado=new double[tam];
		for(int t=0;t<tam;t++){
			resultado[t]=0;
		}
		return resultado;
	}
	public static double[]concatenar(double[]a,double[]b){
		double[]resultado=new double[a.length+b.length];
		System.arraycopy(a,0,resultado,0,a.length);
		System.arraycopy(b,0,resultado,a.length,b.length);
		return resultado;
	}
	public static double[]concatenar(ArrayList<double[]>datos){
		int tam=0;
		for(int i=0;i<datos.size();i++){
			tam+=datos.get(i).length;
		}
		double[]resultado=new double[tam];
		int posicion=0;
		for(int i=0;i<datos.size();i++){
			System.arraycopy(datos.get(i),0,resultado,posicion,datos.get(i).length);
			posicion+=datos.get(i).length;
		}
		return resultado;
	}
	public static double[]derivada(double[]datos){
		double[]resultado=new double[datos.length-1];
		for(int i=0;i<datos.length-1;i++){
			resultado[i]=datos[i+1]-datos[i];
		}
		return resultado;
	}
	public static double[]integral(double[]datos){
		double[]resultado=new double[datos.length];
		double sumatoria=0;
		for(int i=0;i<datos.length;i++){
			sumatoria+=datos[i];
			resultado[i]=sumatoria;
		}
		int k=100;
		for(int i=0;i<resultado.length;i++){
			if(i<resultado.length-k){
				resultado[i]=resultado[i+k]-resultado[i];
			}else{
				resultado[i]=0;
			}
		}
		return resultado;
	}
	public static double[]integral(double[]datos,int l){
		double[]resultado=new double[datos.length];
		double sumatoria=0;
		double min=Double.MAX_VALUE;
		double max=Double.MIN_VALUE;
		for(int i=0;i<datos.length;i++){
			sumatoria+=datos[i];
			resultado[i]=sumatoria;
			if(resultado[i]>max){
				max=resultado[i];
			}
			if(resultado[i]<min){
				min=resultado[i];
			}
			if(i>0&&i%l==0){
				double o=(max+min)/2;
				for(int j=i-l;j<=i;j++){
					resultado[j]=resultado[j]-o*((j-(i-l))/(double)l);
				}
				sumatoria=sumatoria-o;
				min=Double.MAX_VALUE;
				max=Double.MIN_VALUE;
			}
		}
		return resultado;
	}
	public static double[]resta(double[]datos,int k){
		double[]resultado=new double[datos.length];
		for(int i=0;i<datos.length-k;i++){
			resultado[i]=datos[i+k]-datos[i];
		}
		return resultado;
	}
	public static double[]sumar(double[]datosA,double factorA,double[]datosB,double factorB,int desfase){
		int tamano=datosA.length;
		if(datosB.length+desfase>tamano){
			tamano=datosB.length+desfase;
		}
		double[]resultado=new double[tamano];
		for(int i=0;i<tamano;i++){
			double dato=0;
			if(i<datosA.length){
				dato+=factorA*datosA[i];
			}
			if(i>=desfase&&i-desfase<datosB.length){
				dato+=factorB*datosB[i-desfase];
			}
			resultado[i]=dato;
		}
		return resultado;
	}
	public static double[]sumar(double[]a,double[]b){
		return sumar(a,1.,b,1.,0);
	}
	public static double[]sumar(ArrayList<double[]>d){
		int l=0;
		for(int i=0;i<d.size();i++){
			if(l<d.get(i).length){
				l=d.get(i).length;
			}
		}
		double[]r=new double[l];
		for(int i=0;i<d.size();i++){
			for(int t=0;t<d.get(i).length;t++){
				r[t]+=d.get(i)[t];
			}
		}
		return r;
	}
	public static double[]multiplicar(double[]datos,double factor){
		double[]resultado=new double[datos.length];
		for(int i=0;i<resultado.length;i++){
			resultado[i]=factor*resultado[i];
		}
		return resultado;
	}
	public static double[]convolucion(double[]datos,int periodo,int desfase){
		double[]resultado=new double[datos.length];
		for(int t=0;t<datos.length;t++){
			resultado[t]=Math.sin(2*Math.PI*(t+desfase)/periodo)*datos[t];
		}
		return resultado;
	}
	public static double[]promedio3(double[]datos){
		double[]resultado=new double[datos.length];
		for(int i=1;i<datos.length-1;i++){
			resultado[i]=(datos[i-1]+datos[i]+datos[i+1])/3.;
		}
		return resultado;
	}
	public static double[]promedio(double[]datos,int n){
		double[]resultado=new double[datos.length];
		for(int i=0;i<datos.length;i++){
			double t=0;
			int k=0;
			for(int j=i;j<i+n&&j<datos.length;j++){
				t+=datos[j];
				k++;
			}
			resultado[i]=t/k;
		}
		return resultado;
	}
	public static double[]res(double[]d,int x){
		double[]r=new double[d.length/x];
		for(int i=0;i<d.length&&i/x<r.length;i=i+x){
			r[i/x]=d[i];
		}
		return r;
	}
	public static double gf(int n,int j,int dt){
		return 440*Math.pow(2.,((n-69.)+j/((dt*2.+1.)))/12.);
	}
	public static double gp(int n,int j,int dt,double fm){
		return fm/(gf(n,j,dt));
	}
	public static double gp(int n,int j,int dt){
		return gp(n,j,dt,44100);
	}
	public static double gf(int n){
		return gf(n,0,0);
	}
	public static double gp(int n){
		return gp(n,0,0,44100.);
	}
	public static double gn(double f){
		return 12*Math.log(f/440)/Math.log(2)+69;
	}
	public static ArrayList<int[]>maximos(double[]datos){
		ArrayList<int[]>a=new ArrayList<>();
		for(int i=1;i<datos.length-1;i++){
			if(datos[i-1]<datos[i]&&datos[i]>=datos[i+1]&&datos[i]>0){
				a.add(new int[]{i,(int)datos[i]});
			}
		}
		return a;
	}
	public static ArrayList<int[]>minimos(double[]datos){
		ArrayList<int[]>a=new ArrayList<>();
		for(int i=1;i<datos.length-1;i++){
			if(datos[i-1]>datos[i]&&datos[i]<=datos[i+1]){
				a.add(new int[]{i,(int)datos[i]});
			}
		}
		return a;
	}
	public static ArrayList<int[]>maxmins(double[]datos){
		ArrayList<int[]>a=new ArrayList<>();
		for(int i=1;i<datos.length-1;i++){
			if(datos[i-1]<datos[i]&&datos[i]>=datos[i+1]){
				a.add(new int[]{i,(int)datos[i]});
				continue;
			}
			if(datos[i-1]>datos[i]&&datos[i]<=datos[i+1]){
				a.add(new int[]{i,(int)datos[i]});
				continue;
			}
		}
		return a;
	}
	public static ArrayList<int[]>maximos(double[]datos,double max){
		ArrayList<int[]>a=new ArrayList<>();
		for(int i=1;i<datos.length-1;i++){
			if(datos[i-1]<datos[i]&&datos[i]>=datos[i+1]){
				a.add(new int[]{i,(int)(datos[i]*32000./max)});
			}
		}
		return a;
	}
	public static ArrayList<int[]>maximos(ArrayList<int[]>a){
		ArrayList<int[]>r=new ArrayList<int[]>();
		for(int i=1;i<a.size()-1;i++){
			if(a.get(i)[1]>a.get(i-1)[1]&&a.get(i)[1]>a.get(i+1)[1]){
				r.add(a.get(i));
			}
		}
		return r;
	}
	public static int cantidadCiclos(ArrayList<int[]>a,int ini,int nota){
		int r=0;
		int p=a.get(ini)[0];
		for(int i=ini+1;i<a.size();i++){
			int x=(int)Math.round(WGen.gn(44100./(a.get(i)[0]-p)));
			if(x<nota){
				return r;
			}
			if(x==nota){
				r++;
				p=a.get(i)[0];
			}
		}
		return r;
	}
	public static ArrayList<int[]>ciclos(ArrayList<int[]>a,int ini,int nota){
		ArrayList<int[]>r=new ArrayList<int[]>();
		for(int i=ini+1;i<a.size();i++){
			double x=WGen.gn(44100./(a.get(i)[0]-a.get(ini)[0]));
			if(Math.round(x)<nota-1){
				r.add(a.get(ini));
				return r;
			}
			if(Math.abs(x-nota)<=.6){
			//if(Math.round(x)==nota){
				r.add(a.get(ini));
				ini=i;
			}
		}
		return r;
	}
	public static ArrayList<int[]>ciclos(ArrayList<int[]>d,int ini,int a,int b){
		return ciclos(d,ini,(int)Math.round(WGen.gn(44100./(d.get(b)[0]-d.get(a)[0]))));
	}
	public static boolean mayor(ArrayList<int[]>d,int tol){
		for(int i=0;i<d.size();i++){
			if(d.get(i)[1]>tol){
				return true;
			}
		}
		return false;
	}
	public static int longitud(ArrayList<int[]>d){
		return d.get(d.size()-1)[0]-d.get(0)[0];
	}
	public static int gn(ArrayList<int[]>d){
		return(int)Math.round(gn((d.size()-1)*44100./(d.get(d.size()-1)[0]-d.get(0)[0])));
	}
	public static double gn(ArrayList<int[]>a,ArrayList<int[]>b,int ini){
		int k=0;
		for(int i=ini;a.get(i)[0]<b.get(b.size()-1)[0];i++){
			k++;
		}
		double r=gn(k*44100./(b.get(b.size()-1)[0]-a.get(ini)[0]));
		for(int i=ini;a.get(i)[0]<b.get(b.size()-1)[0];i++){
			if(Math.abs(gn(44100./(a.get(i+1)[0]-a.get(i)[0]))-r)>.6){
				return -9999;
			}
		}
		return r;
	}
	public static ArrayList<int[]>ciclos(ArrayList<int[]>a,ArrayList<int[]>b,int ini){
		ArrayList<int[]>r=new ArrayList<int[]>();
		for(int i=ini;a.get(i)[0]<b.get(b.size()-1)[0];i++){
			r.add(a.get(i));
		}
		return r;
	}
	public static double[]reduc(double[]datos,int a,int t){
		double[]b=new double[datos.length/t];
		double max=0;
		for(int i=0;i<datos.length;i++){
			if(i>0&&i%t==0){
				b[i/t]=Math.round(max/a)*a;
				max=0;
			}
			if(datos[i]>max){
				max=datos[i];
			}
		}
		return b;
	}
	public static ArrayList<int[]>amplitudes(double[]datos){
		ArrayList<int[]>r=new ArrayList<>();
		int p=0;
		int t=-1;
		double y=0;
		for(int i=0;i<datos.length-1;i++){
			if(datos[i+1]>datos[i]){
				if(p!=1){
					if(t>=0){
						r.add(new int[]{t,(int)(datos[i]-y)});
					}
					t=i;
					p=1;
					y=datos[i];
				}
				continue;
			}
			if(datos[i+1]<datos[i]){
				if(p!=2){
					if(t>=0){
						r.add(new int[]{t,(int)(datos[i]-y)});
					}
					p=2;
					y=datos[i];
					t=i;
				}
			}
		}
		return r;
	}
	public static double[]toDatos(int l,ArrayList<int[]>d){
		double[]r=new double[l];
		for(int i=0;i<d.size();i++){
			r[d.get(i)[0]]=d.get(i)[1];
		}
		return r;
	}
	public static ArrayList<int[]>simplificarAmplitudes(ArrayList<int[]>d,int min){
		ArrayList<int[]>r=new ArrayList<>();
		int t=-1;
		for(int i=0;i<d.size();i++){
			if(Math.abs(d.get(i)[1])>=min){
				if(t>=0&&(d.get(i)[1]*1.)*r.get(t)[1]>0){
					r.get(t)[1]=r.get(t)[1]+d.get(i)[1];
				}else{
					if(Math.abs(d.get(i)[1])>=min){
						t=r.size();
						r.add(new int[]{d.get(i)[0],d.get(i)[1]});
					}
				}
			}else{
				if(t>=0){
					r.get(t)[1]=r.get(t)[1]+d.get(i)[1];
				}
			}
		}
		return r;
	}
	public static double[]interpolacion(int l,ArrayList<int[]>d){
		double[]r=new double[l];
		int y=0;
		for(int i=0;i<d.size()-1;i++){
			int x=d.get(i+1)[0]-d.get(i)[0];
			for(int t=0;t<x;t++){
				r[d.get(i)[0]+t]=y+d.get(i)[1]*(1/2.-Math.cos(Math.PI*t/x)/2.);
			}
			y+=d.get(i)[1];
		}
		return r;
	}
	public static void interpolacion(double[]r,ArrayList<Double>d,int s,double f){
		int t=0;
		for(int i=0;i<d.size()-1;i++){
			for(int k=0;k<s;k++){
				r[t]+=(k*d.get(i+1)/s+(s-k)*(d.get(i))/s)*Math.sin(2*Math.PI*t*f/44100.);
				t++;
			}
		}
	}
	public static void interpolacion(double[]r,ArrayList<Double>d,ArrayList<Integer>s,double f){
		for(int i=0;i<d.size()-1;i++){
			for(int k=s.get(i);k<s.get(i+1)&&k<r.length;k++){
				r[k]+=(((k-s.get(i))*d.get(i+1)+(s.get(i+1)-k)*d.get(i))/(s.get(i+1)-s.get(i)))*Math.sin(2*Math.PI*k*f/44100.);
			}
		}
	}
	public static void normalizar(double[]r,int a,int b,double v){
		double m=0;
		for(int i=a;i<b&&i<r.length;i++){
			if(Math.abs(r[i])>m){
				m=Math.abs(r[i]);
			}
		}
		for(int i=a;i<b&&i<r.length;i++){
			r[i]=v/m*r[i];
		}
	}
	public static double[]e(double[]d){
		double[]r=new double[d.length];
		for(int i=0;i<d.length-1;i++){
			double v=d[i+1]-d[i];
			r[i]=d[i]*d[i]+v*v;
		}
		return r;
	}
	public static double[]nms(double[]datos,int a,int b,int ni,int nf,int dt){
		return nms(datos,a,b,Masa.rango(ni,nf,dt));
	}
	public static double[]nms(double[]datos,int a,int b,int ni,int nf,int dt,int na){
		return nms(datos,a,b,Masa.rango(ni,nf,dt,na));
	}
	public static double[]nms(double[]datos,int ni,int nf,int dt){
		return nms(datos,0,datos.length,ni,nf,dt);
	}
	public static double[]nms(double[]datos,int ni,int nf,int dt,int na){
		return nms(datos,0,datos.length,ni,nf,dt,na);
	}
	public static double[]nms(double[]datos,int a,int b,ArrayList<Masa>m){
		double[]r=new double[b-a];
		for(int t=a;t<b;t++){
			for(int i=0;i<m.size();i++){
				m.get(i).act(datos[t]);
			}
			double z=0;
			for(int i=0;i<m.size();i++){
				z+=m.get(i).x;
			}
			r[t-a]=z;
		}
		return r;
	}
	public static boolean ver(double[]datos,int a,int b,double f,double t){
		Masa m=new Masa(f);
		m.cx=.995;
		int pa=-1;
		int pb=0;
		for(int i=a;i<b;i++){
			m.act(datos[i]);
			if(m.x2<m.x1&&m.x1>m.x){
				if(pa==-1){
					pa=i-1;
				}
				pb=i-1;
			}
			if(m.x2>m.x1&&m.x1<m.x){
				if(pa==-1){
					pa=i-1;
				}
				pb=i-1;
			}
			if(m.o>=5){
				if((4.*(pb-pa-1))/m.o<44100/f&&(4.*(pb-pa+1))/m.o>44100/f){
				}else{
					return false;
				}
			}
		}
		return true;
	}
	public static long[]maxsum(double[]datos,int b,int s1,int s2,int n,int min,int max){
		double mx=0;
		long p=0;
		long l=0;
		for(int i=s1;i<datos.length&&i<s2;i++){
			for(int j=min;j<=max&&i+(n-1)*j<datos.length&&i+(n-1)*j<b;j++){
				double t=0;
				for(int k=0;k<n;k++){
					int f=-1;
					if(k%2==0){
						f=1;
					}
					t+=f*datos[i+k*j];
				}
				double v=Math.abs(t);
				if(v>mx){
					mx=v;
					p=i;
					l=j;
				}
			}
		}
		return new long[]{p,l};
	}
	public static double disi(int[]a,int[]b){
		double t=0;
		for(int i=0;i<a.length;i++){
			t+=(a[i]-b[i])*(a[i]-b[i]);
		}
		return Math.sqrt(t);
	}
	public static int[]resta(int[]a,int[]b){
		int[]r=new int[a.length];
		for(int i=0;i<r.length;i++){
			r[i]=a[i]-b[i];
		}
		return r;
	}
	public static String mos(int[]a){
		StringBuilder r=new StringBuilder();
		r.append("(");
		r.append(a[0]);
		for(int i=1;i<a.length;i++){
			r.append(",");
			r.append(a[i]);
		}
		r.append(")");
		return r+"";
	}
	public static int gnr(int lc,double fm){
		double f=fm/lc;
		return(int)Math.round(gn(f));
	}
	public static void act0(double[]d,Masa m,int a,int b){
		for(int i=a;i<b;i++){
			m.act(0);
			d[i]+=m.x;
		}
	}
}