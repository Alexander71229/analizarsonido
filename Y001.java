import java.util.*;
import java.io.*;
import static c.U.imp;
class R{
	public int i;
	public double r;
	public int n;
	public int o;
	public int lc;
	public String toString(){
		return"{"+i+","+r+","+n+","+p()+"}";
	}
	public double p(){
		return r/n;
	}
}
public class Y001{
	public static R sim(ArrayList<int[]>d,int ix,int lmn,int lmx){
		int ini=-1;
		for(int i=ix+1;i<d.size()&&d.get(i)[0]-d.get(ix)[0]<=lmx;i++){
			if(d.get(i)[0]-d.get(ix)[0]>=lmn){
				ini=i;
				break;
			}
		}
		if(ini==-1){
			return null;
		}
		double mn=10000000;
		boolean nv=false;
		int xmx,xmn;
		R r=null;
		for(int i=ini;i<d.size()&&d.get(i)[0]-d.get(ix)[0]<=lmx;i++){
			double t=0;
			double pm=0;
			int n=0;
			nv=false;
			for(int j=i,k=ix;d.get(k)[0]<d.get(i)[0];j++,k++){
				if(j>=d.size()){
					nv=true;
					break;
				}
				double x=1;
				int dn=d.get(j)[0]-d.get(i)[0];
				if(dn>0){
					x=(d.get(k)[0]*1.-d.get(ix)[0])/dn;
				}
				if(0.8<=x&&x<=1.2){
					t+=WGen.disi(WGen.resta(d.get(k),new int[]{d.get(ix)[0],0}),WGen.resta(d.get(j),new int[]{d.get(i)[0],0}));
					n++;
				}else{
					if(d.get(j)[0]-d.get(i)[0]>d.get(k)[0]-d.get(ix)[0]){
						j--;
					}else{
						k--;
					}
				}
			}
			if(!nv){
				pm=t/n;
				if(pm<mn){
					mn=pm;
					if(r==null){
						r=new R();
					}
					r.i=i;
					r.r=t;
					r.n=n;
					r.o=ix;
					r.lc=d.get(r.i)[0]-d.get(r.o)[0];
				}
			}
		}
		return r;
	}
	public static int stm(ArrayList<int[]>mns,int ix,int bmt,int amt){
		int imx=0,imn=0;
		int mx=-100000,mn=100000;
		for(int i=ix;i<mns.size();i++){
			if(mns.get(i)[0]-mns.get(imn)[0]>bmt&&mx-Math.max(mn,mns.get(i)[1])>amt&&mns.get(imn)[0]<mns.get(imx)[0]&&mns.get(imx)[0]<mns.get(i)[0]){
				mn=100000;
				return imx;
			}
			if(mns.get(i)[0]-mns.get(imx)[0]>bmt&&Math.min(mx,mns.get(i)[1])-mn>amt&&mns.get(imx)[0]<mns.get(imn)[0]&&mns.get(imn)[0]<mns.get(i)[0]){
				mx=-100000;
				return imn;
			}
			if(mx<mns.get(i)[1]){
				mx=mns.get(i)[1];
				imx=i;
			}
			if(mn>mns.get(i)[1]){
				mn=mns.get(i)[1];
				imn=i;
			}
		}
		return-1;
	}
	public static int amp(ArrayList<int[]>mns,int ix,int ln){
		int mn=100000,mx=-100000;
		for(int i=ix;i<mns.size()&&i<ix+ln;i++){
			if(mx<mns.get(i)[1]){
				mx=mns.get(i)[1];
			}
			if(mn>mns.get(i)[1]){
				mn=mns.get(i)[1];
			}
		}
		return mx-mn;
	}
	public static boolean sil(ArrayList<int[]>mns,int ix,int ln,int amt){
		if(amp(mns,ix,ln)<amt){
			return true;
		}
		return false;
	}
	public static double pmd(ArrayList<int[]>mns,ArrayList<R>rs,int ix,int ln){
		return(ln*1.)/(mns.get(rs.get(ix+ln-1).i)[0]-mns.get(rs.get(ix).o)[0]);
	}
	public static void main(String[]argumentos){
		try{
			imp("Inicio");
			double[]datos=new Wave(new File("F:\\Desarrollo\\Audio\\Wav\\All of me-vocals.wav")).getms();
			for(int i=1;i<=3;i++){
				datos=WGen.promedio3(datos);
			}
			ArrayList<int[]>mns=WGen.maxmins(datos);
			int e=0;
			int inx=0;
			int amt=2000,bmt=50,ln=400;
			boolean act=true;
			ArrayList<R>rs=new ArrayList<>();
			//R rx=sim(mns,4254,60,400);
			//imp(""+mns.get(rx.o)[0]+"<>"+mns.get(rx.i)[0]);
			//System.exit(0);
			while(act){
				if(e==0){
					inx=stm(mns,inx,bmt,amt);
					e=1;
					if(inx==-1){
						act=false;
						break;
					}
				}
				if(e==1){
					if(sil(mns,inx,ln,amt)){
						e=0;
						continue;
					}
					R r=sim(mns,inx,60,400);
					rs.add(r);
					if(r==null){
						act=false;
						break;
					}
					inx=r.i;
				}
			}
			for(int i=0;i<rs.size();i++){
				imp(rs.get(i).o+":"+mns.get(rs.get(i).o)[0]+">"+rs.get(i).p());
			}
			double[]dat=new double[0];
			for(int i=0;i<rs.size();i++){
				int l=mns.get(rs.get(i).i)[0]-mns.get(rs.get(i).o)[0];
				int a=amp(mns,rs.get(i).o,rs.get(i).i+1-rs.get(i).o);
				imp(a+":"+l+">"+mns.get(rs.get(i).o)[0]+"<>"+mns.get(rs.get(i).i)[0]);
				dat=WGen.concatenar(dat,WGen.sinusoide(a,l,0,l));
			}
			Wave.p(dat,"All-test1.wav");
			/*
			Midin mdn=new Midin(44100);
			inx=0;
			boolean cmb=false;
			for(int i=0;i<rs.size();i++){
				if(i>0){
					if(rs.get(i).o!=rs.get(i-1).i){
						cmb=true;
					}
				}
				if(mns.get(rs.get(i).i)[0]-mns.get(rs.get(inx).o)[0]>=4410){
					cmb=true;
				}
				if(cmb){
					int n=(int)Math.round(WGen.gn(44100.*pmd(mns,rs,inx,i-inx+1)));
					if(n>0){
						mdn.add(n,100,mns.get(rs.get(inx).o)[0],mns.get(rs.get(i).i)[0]);
					}
					imp(n+">"+mns.get(rs.get(inx).o)[0]+":"+mns.get(rs.get(i).i)[0]+"("+inx+","+i+")");
					cmb=false;
					inx=i+1;
				}
			}
			mdn.write("All.mid",0);
			*/
			/*int cnt=0;
			boolean cmb=false;
			inx=0;
			Midiw mdw=new Midiw("all.mid");
			for(int i=0;i<rs.size();i++){
				//imp(""+WGen.gn(44100.*pmd(mns,rs,i,10)));
				if(i>0){
					if(rs.get(i).o!=rs.get(i-1).i){
						cmb=true;
					}
				}
				if(rs.get(i).p()>5000){
					cmb=true;
				}
				if(cmb){
					if(cnt>0){
						int n=(int)Math.round(WGen.gn(44100.*pmd(mns,rs,inx,cnt)));
						mdw.add(n,100,mns.get(rs.get(inx).o)[0],mns.get(rs.get(i).i)[0]);
					}
					cmb=false;
					cnt=0;
					inx=i;
				}
				cnt++;
			}
			mdw.write(0,441);*/
			/*Midin mdn=new Midin(44100);
			for(int i=0;i<rs.size();i++){
				if(rs.get(i).p()<1000||(i>0&&rs.get(i-1).p()<1000)){
					int n=WGen.gnr(rs.get(i).lc,44100.);
					mdn.add(n,100,mns.get(rs.get(i).o)[0],mns.get(rs.get(i).i)[0]);
				}
			}
			mdn.write("All.mid",0);*/
			imp("Fin");
		}catch(Throwable t){
			imp(t);
		}
	}
}