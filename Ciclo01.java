import java.util.*;
import java.io.*;
import java.awt.*;
import java.awt.image.*;
import javax.imageio.*;
public class Ciclo01{
	public static HashMap<String,int[]>gh=new HashMap<>();
	public static int[]lim(ArrayList<int[]>ps,int n,int lx){
		if(gh.get(ps.get(lx)[0]+":"+n)!=null){
			return gh.get(ps.get(lx)[0]+":"+n);
		}
		ArrayList<int[]>tr=new ArrayList<>();
		tr.add(ps.get(lx));
		int[]r=new int[3];
		int l=lx;
		r[2]=ps.get(lx)[1];
		r[0]=ps.get(lx)[0];
		r[1]=ps.get(lx)[0];
		for(int i=l+1;i<ps.size();){
			int x=(int)Math.round(WGen.gn(44100./(ps.get(i)[0]-ps.get(l)[0])));
			if(x<n){
				break;
			}
			if(x==n){
				tr.add(ps.get(i));
				r[1]=ps.get(i)[0];
				if(r[2]<ps.get(i)[1]){
					r[2]=ps.get(i)[1];
				}
				l=i;
				i=l+1;
				continue;
			}
			i++;
		}
		l=lx;
		for(int i=l-1;i>=0;){
			int x=(int)Math.round(WGen.gn(44100./(ps.get(l)[0]-ps.get(i)[0])));
			if(x<n){
				break;
			}
			if(x==n){
				tr.add(ps.get(i));
				r[0]=ps.get(i)[0];
				if(r[2]<ps.get(i)[1]){
					r[2]=ps.get(i)[1];
				}
				l=i;
				i=l-1;
				continue;
			}
			i--;
		}
		if(n==45||true){
			c.U.imp("X"+n+":"+ps.get(lx)[0]);
		}
		for(int i=0;i<tr.size();i++){
			if(n==45||true){
				if(i<tr.size()-1){
					c.U.imp(n+"--->"+tr.get(i)[0]+":"+tr.get(i)[1]+"||:"+(Math.min(tr.get(i)[1],tr.get(i+1)[1])/(double)Math.max(tr.get(i)[1],tr.get(i+1)[1])));
				}else{
					c.U.imp(n+"--->"+tr.get(i)[0]+":"+tr.get(i)[1]);
				}
			}
			gh.put(tr.get(i)[0]+":"+n,r);
		}
		return r;
	}
	public static void main(String[]args){
		try{//1000 //2275 //8453
			System.out.println("Inicio");
			c.U.imp("Inicio");
			String nombre="D:\\Wavs\\Seek and Destroy-Bass2-Musical28_28_0_1";
			Midiw m=new Midiw(nombre);
			m.sa=new int[0];
			double[]datos=new Wave(new File(nombre+".wav")).getms();
			double[]d=new double[datos.length];
			ArrayList<int[]>a=new ArrayList<>();
			int max=0;
			int lx=400;
			int lm=200;
			int k=2;
			int nm=28;
			for(int i=1;i<datos.length-1;i++){
				if(datos[i-1]<datos[i]&&datos[i]>=datos[i+1]&&datos[i]>0){
					d[i]=datos[i];
					a.add(new int[]{i,(int)d[i]});
					if(max<d[i]){
						max=(int)d[i];
					}
				}
			}
			int f=0;
			int m0=max/10;
			int tini=0;
			ArrayList<int[]>ps=new ArrayList<>();
			for(int i=0;i<a.size();i++){
				if(a.get(i)[1]<=m0){
					continue;
				}
				int j=i-1;
				boolean p=true;
				while(j>0&&a.get(i)[0]-a.get(j)[0]<=lm){
					if(a.get(i)[1]<a.get(j)[1]){
						p=false;
					}
					j--;
				}
				if(p){
					j=i+1;
					while(j<a.size()&&a.get(j)[0]-a.get(i)[0]<=lm){
						if(a.get(i)[1]<a.get(j)[1]){
							p=false;
						}
						j++;
					}
				}
				if(p){
					ps.add(a.get(i));
				}
			}
			HashMap<String,Integer>hn=new HashMap<>();
			for(int i=0;i<ps.size()-k;i++){
				int na=0;
				for(int j=1;j<=k;j++){
					int n=(int)Math.round(WGen.gn(44100./(ps.get(i+j)[0]-ps.get(i)[0])));
					if(j==1){
						na=n;
					}
					if(n<nm){
						continue;
					}
					if(j>1&&na-n>=12){
						break;
					}
					int[]r=lim(ps,n,i);
					if(r[1]-r[0]>lx&&hn.get(r[0]+":"+r[1])==null){
						hn.put(r[0]+":"+r[1],1);
						c.U.imp(n+"->"+r[0]+"<>"+r[1]+":"+r[2]+"="+(r[1]-r[0]));
						m.add(n,r[2],r[0],r[1]);
					}
				}
			}
			m.write();
			//Wave.p(d,nombre+"-c01.wav");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}