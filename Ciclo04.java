import java.io.*;
import java.util.*;
public class Ciclo04{
	public static void main(String[]argumentos){
		try{
			System.out.println("Inicio");
			c.U.imp("Inicio");
			String nombre="D:\\Wavs\\Seek and Destroy-Bass2";
			Wave w=new Wave(new File(nombre+".wav"));
			double[]datoso=w.getms(0,44100*60);
			Midiw m=new Midiw(nombre);
			m.sa=new int[0];
			int ni=33;
			int nf=33;
			int dt=0;
			int na=3;
			Masa.coe=0.995;
			double gmax=0;
			for(int pni=ni;pni<=nf;pni++){
				double[]datos=new double[datoso.length];
				System.arraycopy(datoso,0,datos,0,datoso.length);
				ArrayList<Masa>masa=Masa.rango(pni,pni,dt,na);
				double max=0;
				for(int k=1;k<=2;k++){
					for(int j=0;j<masa.size();j++){
						masa.get(j).res();
					}
					for(int i=0;i<datos.length;i++){
						double t=0;
						for(int j=0;j<masa.size();j++){
							masa.get(j).act(datos[i]);
							t=t+masa.get(j).x*masa.get(j).f;
							//t=t+masa.get(j).x;
						}
						datos[i]=t;
						if(t>max){
							max=t;
						}
					}
				}
				if(gmax<max){
					gmax=max;
				}
				double[]d=new double[datos.length];
				ArrayList<int[]>a=WGen.maximos(datos,max);
				Wave.p(datos,nombre+"-c4a.wav");
				for(int i=0;i<a.size();i++){
					d[a.get(i)[0]]=a.get(i)[1];
				}
				Wave.p(d,nombre+"-c4b.wav");
				ArrayList<int[]>notas=new ArrayList<>();
				int pini=-1;
				int val=0;
				int e=0;
				int x=0;
				int pfin=-1;
				int nc=0;
				for(int i=0;i<a.size()-na;i++){
					if(pini>=0&&((((i-pini)%na==0)&&Math.round(WGen.gn(44100./(a.get(i)[0]-a.get(x)[0])))==pni)||(((i-pini)%na>0)&&(a.get(x)[1]>a.get(i)[1])))){
						if((i-pini)%na==0){
							x=i;
							nc++;
							pfin=i;
							if(a.get(i)[1]>val){
								val=a.get(i)[1];
							}
							/*if(val>50&&a.get(pfin)[0]-a.get(pini)[0]>30){*/
							if(nc>=3){
								e=2;
							}
						}
					}else{
						if(e==2){
							notas.add(new int[]{a.get(pini)[0],a.get(pfin)[0],val});
						}
						pini=i;
						x=i;
						val=a.get(i)[1];
						e=1;
						nc=0;
					}
				}
				ArrayList<int[]>notasResultado=new ArrayList<>();
				for(int i=0;i<notas.size();i++){
					if((i<notas.size()-1)&&(Math.abs(WGen.gn(44100./(notas.get(i+1)[0]-notas.get(i)[1]))-pni)<.7)){
						notas.add(new int[]{notas.get(i)[0],notas.get(i+1)[1],Math.max(notas.get(i+1)[2],notas.get(i)[2])});
						i++;
					}else{
						notasResultado.add(notas.get(i));
					}
				}
				for(int i=0;i<notasResultado.size();i++){
					if(notasResultado.get(i)[2]>5000&&(notasResultado.get(i)[1]-notasResultado.get(i)[0]>1500)){
						c.U.imp(notasResultado.get(i)[0]+"<>"+notasResultado.get(i)[1]);
						m.add(pni,notasResultado.get(i)[2],notasResultado.get(i)[0],notasResultado.get(i)[1]);
					}
				}
			}
			m.write();
			c.U.imp("gmax:"+gmax);
		}catch(Throwable t){
			c.U.imp(t);
		}
	}
}