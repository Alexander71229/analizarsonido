import java.util.*;
import java.io.*;
import java.awt.*;
import java.awt.image.*;
import javax.imageio.*;
public class Melody2{
	public static void main(String[]args){
		try{
			c.U.imp("Inicio");
			String nombre="C:\\Desarrollo\\Audio\\wavs\\Silent02";
			Wave w=new Wave(new File(nombre+".wav"));
			Midiw md=new Midiw(nombre+".mid");
			double[]datos=w.getms();
			double[]s=new double[datos.length];
			int ni=52;
			int nf=67;
			int dt=20;
			int na=5;
			ArrayList<Masa>ms=Masa.rango(ni,nf,dt,1);
			ArrayList<Masa>nz=Masa.rango(ni,ni,0,na);
			int l=4410*2;
			int n=-1;
			int an=-1;
			for(int i=0;i<datos.length-l;i+=l){
				Masa.resx(ms);
				Masa.coe=1;
				Masa.g(datos,ms,i,i+l);
				Masa.coe=0.995;
				int im=Masa.max(ms);
				if(im>0&&i<datos.length-1){
					n=ms.get(im).n;
					if(n!=an){
						for(int j=1;j<=na;j++){
							nz.get(j-1).setF(j*ms.get(im).f);
						}
						if(an>-1){
							md.fin(an,i);
						}
						an=n;
					}
					md.ini(n,ms.get(im).m,i);
					Masa.g(datos,s,nz,i,i+l);
				}else{
					if(an>-1){
						md.fin(an,i);
						an=-1;
					}
					Masa.z(s,nz,i,i+l);
				}
			}
			if(an>-1){
				md.fin(an,datos.length);
			}
			//Wave.p(WGen.sumar(s,.2,datos,1,0),nombre+"-Melody2"+ni+"_"+nf+"_"+dt+"_"+na+".wav");
			Wave.p(s,nombre+"-Melody2"+ni+"_"+nf+"_"+dt+"_"+na+".wav");
			md.write();
			c.U.imp("Fin");
		}catch(Exception e){
			c.U.imp(e);
		}
	}
}