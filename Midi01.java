import java.util.*;
import java.io.*;
import java.awt.*;
import java.awt.image.*;
import javax.imageio.*;
class P{
	public int t;
	public double v;
	public P(int t,double v){
		this.t=t;
		this.v=v;
	}
}
public class Midi01{
	public static void main(String[]args){
		try{
			c.U.imp("Inicio");
			//D:\Wavs\
			//String nombre="D:\\Wavs\\Seek and Destroy-Bass2";
			//String nombre="D:\\Wavs\\Cho01";
			String nombre="D:\\Wavs\\AntMr";
			//String nombre="D:\\Wavs\\Al Otro Lado del Silencio-Instrumental";
			Wave w=new Wave(new File(nombre+".wav"));
			double[]datos=w.getms();
			int ni=43;
			int nf=62;
			int dt=0;
			int z=10;
			double xf=Math.pow(2.,1./12.);
			double zf=Math.pow(2.,1./3.);
			ArrayList<Masa>masa=Masa.rango(ni,nf,dt);
			int[]os=new int[masa.size()];
			ArrayList<P>[]ps=new ArrayList[masa.size()];
			for(int i=0;i<ps.length;i++){
				ps[i]=new ArrayList<>();
			}
			Masa.coe=0.995;
			Midiw m=new Midiw(nombre);
			double mx=0;
			for(int i=0;i<datos.length;i++){
				for(int j=0;j<masa.size();j++){
					masa.get(j).act(datos[i]);
					if(mx<masa.get(j).m){
						mx=masa.get(j).m;
					}
					if(masa.get(j).o!=os[j]){
						ps[j].add(new P(i-1,(masa.get(j).ax1)));
						os[j]=masa.get(j).o;
					}
				}
				datos[i]=0;
				for(int j=0;j<masa.size();j++){
					datos[i]+=masa.get(0).x;
				}
			}
			Wave.p(datos,nombre+"-Midi01"+ni+"_"+nf+"_"+dt+".wav");
			for(int j=0;j<masa.size();j++){
				int g=0;
				double zmx=0;
				for(int i=0;i<ps[j].size();i++){
					boolean u=true;
					if(i>0&&ps[j].get(i).v*ps[j].get(i-1).v>=0){
						u=false;//c.U.imp("1");
					}
					if(Math.abs(ps[j].get(i).v)<masa.get(j).m/10.){
						u=false;//c.U.imp("2");
					}
					if(i>0&&(ps[j].get(i).t-ps[j].get(i-1).t<(22050/masa.get(j).f)/zf||ps[j].get(i).t-ps[j].get(i-1).t>(22050/masa.get(j).f)*zf)){
						u=false;//c.U.imp("3:"+(ps[j].get(i).t-ps[j].get(i-1).t)+"<>"+(22050/masa.get(j).f)/zf+":"+(22050/masa.get(j).f)*zf);
					}
					//c.U.imp(masa.get(j).n+":"+ps[j].get(i).t+":"+ps[j].get(i).v+"->"+u);
					//c.U.imp(i+":"+g+":"+z);
					if(u){
						g++;
						if(zmx<Math.abs(ps[j].get(i).v)){
							zmx=Math.abs(ps[j].get(i).v);
						}
						if(g>z){
							if(ps[j].get(i).t-ps[j].get(i-z).t>=(22050*z/masa.get(j).f)/xf&&ps[j].get(i).t-ps[j].get(i-z).t<=(22050*z/masa.get(j).f)*xf){
								//c.U.imp("-->"+ps[j].get(i).t+"<>"+ps[j].get(i-z).t+":"+(ps[j].get(i).t-ps[j].get(i-z).t));
								//c.U.imp((22050*z/masa.get(j).f)/xf+"<>"+(22050*z/masa.get(j).f)*xf);
								m.ini(masa.get(j).n,zmx,ps[j].get(i-z).t);
							}
						}
					}else{
						//c.U.imp("z:"+ps[j].get(i).t);
						m.fin(masa.get(j).n,ps[j].get(i).t);
						g=0;
						zmx=0;
					}
				}
			}
			m.write();
			c.U.imp("max:"+mx);
		}catch(Exception e){
			c.U.imp(e);
		}
	}
}