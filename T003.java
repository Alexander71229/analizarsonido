import java.util.*;
import java.io.*;
import static c.U.imp;
public class T003{
	public static void main(String[]argumentos){
		try{
			c.U.ruta="log.txt";
			imp("Inicio");
			//String nombre="H:\\Desarrollo\\Audio\\Coro\\ProLírica\\Carmen\\Mp3\\TTS\\S02";
			String nombre="F:\\Desarrollo\\Audio\\Wav\\Cum";
			double[]datos=new Wave(new File(nombre+".wav")).getms();
			double[]r=new double[datos.length];
			int ni=72;
			int nf=72;
			int dt=0;
			int na=20;
			Masa.coe=0.995;
			ArrayList<Masa>m=Masa.rango(ni,nf,dt,na);
			int[]x=new int[m.size()];
			ArrayList<ArrayList<Double>>d=new ArrayList<>();
			ArrayList<ArrayList<Integer>>s=new ArrayList<>();
			for(int i=0;i<x.length;i++){
				x[i]=1;
				d.add(new ArrayList<>());
				d.get(i).add(.0);
				s.add(new ArrayList<>());
				s.get(i).add(0);
			}
			for(int t=0;t<datos.length;t++){
				for(int i=0;i<m.size();i++){
					m.get(i).act(datos[t]);
					if(t>=x[i]*44100./m.get(i).f){
						d.get(i).add(30000*m.get(i).m);
						s.get(i).add(t);
						x[i]++;
						m.get(i).m=0;
					}
				}
			}
			for(int i=0;i<d.size();i++){
				WGen.interpolacion(r,d.get(i),s.get(i),m.get(i).f);
			}
			Wave.p(r,nombre+"-T003.wav");
			imp("Fin");
		}catch(Throwable t){
			imp(t);
		}
	}
}