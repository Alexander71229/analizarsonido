import java.io.*;
import java.util.*;
public class Ciclo02{
	public static void main(String[]argumentos){
		try{
			System.out.println("Inicio");
			c.U.imp("Inicio");
			String nombre="D:\\Wavs\\Seek and Destroy-Bass2";
			Midiw m=new Midiw(nombre);
			m.sa=new int[0];
			double[]datoso=new Wave(new File(nombre+".wav")).getms(0,44100*60);
			double[]datos=new double[datoso.length];
			
			int ni=33;
			int nf=33;
			int dt=0;
			int na=1;
			Masa.coe=0.995;
			ArrayList<Masa>masa=Masa.rango(ni,nf,dt,na);
			double[]tx=new double[datos.length];
			double tol=0;
			for(int i=0;i<masa.size();i++){
				System.arraycopy(datoso,0,datos,0,datoso.length);
				double mx=0;
				double l=44100./masa.get(i).f;
				for(int cx=1;cx<=5;cx++){
					for(int t=0;t<datos.length;t++){
						masa.get(i).act(datos[t]);
						tx[t]=masa.get(i).x;
						datos[t]=tx[t];
						if(tx[t]>mx){
							mx=tx[t];
						}
					}
				}
				Wave.p(datos,nombre+"-c2.wav");
				tol=5000;
				ArrayList<int[]>a=new ArrayList<>();
				for(int t=1;t<tx.length-1;t++){
					if(tx[t-1]<tx[t]&&tx[t]>=tx[t+1]&&tx[t]>0){
						a.add(new int[]{t,(int)(tx[t]/mx*32000.)});
					}
				}
				int e=0;
				int pini=0;
				int pfin=-3000;
				int pval=0;
				int cc=0;
				for(int j=0;j<a.size();j++){
					boolean ec=false;
					if(a.get(j)[1]<=tol){
						continue;
					}
					for(int k=j+1;k<a.size()&&k<=j+2;k++){
						double f=44100./(a.get(k)[0]-a.get(j)[0]);
						int n=(int)Math.round(WGen.gn(f));
						if(n<masa.get(i).n||n-masa.get(i).n==12||(a.get(k)[0]-a.get(j)[0])/l<.6){
							break;
						}
						if(a.get(k)[1]<=tol){
							continue;
						}
						if(n==masa.get(i).n){
							ec=true;
							cc++;
							if(pval<a.get(j)[1]){
								pval=a.get(j)[1];
							}
							pfin=a.get(k)[0];
							break;
						}
					}
					if(ec){
						if(e==0){
							e=1;
							pini=a.get(j)[0];
						}
					}else{
						if(e==1){
							if(a.get(j)[0]-pfin>l||true){
								e=0;
								/*c.U.imp(pini+"<->"+pfin+"->"+(pfin-pini));
								c.U.imp("cc:"+cc);
								c.U.imp(":"+WGen.gn((cc)*44100./(pfin-pini)));*/
								if((pfin-pini)>l*1.5&&Math.round(WGen.gn((cc)*44100./(pfin-pini)))==masa.get(i).n){
								//if((pfin-pini)>1000&&Math.round(WGen.gn((cc)*44100./(pfin-pini)))==masa.get(i).n){
								//if((pfin-pini)>10){
									c.U.imp(pini+"<->"+pfin+"->"+(pfin-pini));
									m.add(masa.get(i).n,pval,pini,pfin);
								}
								pval=0;
								cc=0;
							}
						}
					}
				}
			}
			m.write();
		}catch(Throwable t){
			c.U.imp(t);
		}
	}
}