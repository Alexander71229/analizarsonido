import java.util.*;
import java.io.*;
import java.awt.*;
import java.awt.image.*;
import javax.imageio.*;
public class SpecPrint{
	public static double mx=0;
	public static BufferedImage gen(double[][]d2d,double mx)throws Exception{
		BufferedImage bi=new BufferedImage(d2d[0].length,d2d.length,BufferedImage.TYPE_INT_RGB);
		Graphics g=bi.getGraphics();
		for(int i=0;i<d2d.length;i++){
			for(int j=0;j<d2d[i].length;j++){
				//int v=(int)(16777215.*(Math.abs(d2d[i][j])/mx));
				int v=0;
				/*if(i>0&&i<d2d.length-1){
					if(d2d[i-1][j]<d2d[i][j]&&d2d[i][j]<d2d[i+1][j]){
						v=(int)(255.*(Math.abs(d2d[i][j])/mx));
						//v=(int)(16777214.*(Math.abs(d2d[i][j])/mx));
					}
				}*/
				v=(int)(255.*(Math.abs(d2d[i][j])/mx));
				g.setColor(new Color(v));
				g.drawLine(j,d2d.length-1-i,j,d2d.length-1-i);
			}
		}
		return bi;
	}
	public static double gmx(double[][]d2d,int a,int b){
		double r=0;
		for(int i=0;i<d2d.length;i++){
			for(int j=a;j<b&&j<d2d[i].length;j++){
				if(r<d2d[i][j]){
					r=d2d[i][j];
				}
			}
		}
		return r;
	}
	public static double gmx(double[][]d2d){
		double r=0;
		for(int i=0;i<d2d.length;i++){
			for(int j=0;j<d2d[i].length;j++){
				if(r<d2d[i][j]){
					r=d2d[i][j];
				}
			}
		}
		return r;
	}
	public static BufferedImage gen(double[][]d2d)throws Exception{
		BufferedImage bi=new BufferedImage(d2d[0].length,d2d.length,BufferedImage.TYPE_INT_RGB);
		Graphics g=bi.getGraphics();
		double mx=0;
		int k=2;
		for(int i=0;i<d2d.length;i++){
			for(int j=0;j<d2d[i].length;j++){
				if(mx==0){
					mx=gmx(d2d,j,j+k);
				}
				int v=(int)(255.*(Math.abs(d2d[i][j])/mx));
				if(j%k==0&&j>0){
					mx=0;
				}
				g.setColor(new Color(v));
				g.drawLine(j,d2d.length-1-i,j,d2d.length-1-i);
			}
		}
		return bi;
	}
	public static BufferedImage gen(double[][]d2d,int ni,int nf,int dt)throws Exception{
		BufferedImage bi=new BufferedImage(d2d[0].length,d2d.length,BufferedImage.TYPE_INT_RGB);
		Graphics g=bi.getGraphics();
		double mx=0;
		int k=2;
		HashMap<Integer,Integer>h=new HashMap<>();
		h.put(1,1);
		h.put(3,1);
		h.put(6,1);
		h.put(8,1);
		h.put(10,1);
		for(int i=0;i<d2d.length;i++){
			for(int j=0;j<d2d[i].length;j++){
				if(mx==0){
					mx=gmx(d2d,j,j+k);
				}
				int v=(int)(255.*(Math.abs(d2d[i][j])/mx));
				if(v>255){
					v=255;
				}
				if(j%k==0&&j>0){
					mx=0;
				}
				if(h.get((ni+i/(dt*2+1))%12)!=null){
					g.setColor(new Color(40,0,v));
				}else{
					g.setColor(new Color(v));
				}
				g.drawLine(j,d2d.length-1-i,j,d2d.length-1-i);
			}
		}
		return bi;
	}
	public static double[][]v2d(double[]datos,int tini,int tfin,int ni,int nf,int dt)throws Exception{
		ArrayList<Masa>masas=Masa.rango(ni,nf,dt);
		double[][]xms=new double[masas.size()][tfin-tini];
		for(int t=tini;t<tfin;t++){
			for(int i=0;i<masas.size();i++){
				//xms[i][t-tini]=(masas.get(i).x*masas.get(i).x+masas.get(i).v*masas.get(i).v)/masas.get(i).w;
				//double p=44100./masas.get(i).f;
				double p=22050./masas.get(i).f;
				int h=(int)Math.floor(t/p);
				if(t-h*p<p/2.){
					//xms[i][t-tini]=2;
				}else{
					//xms[i][t-tini]=0;
				}
				if(t>0&&(int)Math.floor(t/p)!=(int)Math.floor((t-1)/p)){
					for(int k=t;k>0&&k>=t-p;k--){
						//xms[i][k]=masas.get(i).m;
					}
					//masas.get(i).m=0;
					//xms[i][t-tini]=0;
				}else{
					//xms[i][t-tini]=1;
				}
				int u=1;
				if(t>0&&t%u==0){
					for(int k=t;k>0&&k>=t-u;k--){
						xms[i][k]=masas.get(i).m;
					}
				}
				if(Math.abs(xms[i][t-tini])>mx){
					mx=Math.abs(xms[i][t-tini]);
				}
				masas.get(i).act(datos[t]);
			}
		}
		return xms;
	}
}