import java.util.*;
import java.io.*;
import java.awt.*;
import java.awt.image.*;
import javax.imageio.*;
import c.U;
class C{
	int a;
	int b;
	int c;
	double t;
	int s=1;
	public C(int a,int b,double[]r){
		this.a=a;
		this.b=b;
		this.c=2;
		this.t=r[a]-r[b];
	}
	public boolean add(int a,int b,double[]r){
		if(this.b!=a){
			return false;
		}
		if(t*(t+this.s*r[b])<=0){
			return false;
		}
		if(Midi02.cx<=this.c){
			if(Math.abs(this.t/this.c)<Midi02.tol){
				return false;
			}
			if(!WGen.ver(r,this.a,this.b,(22050./((this.b-this.a*1.)/this.c)),0)){
				return false;
			}
		}
		double mn=(this.b-this.a-.5)/(this.c-1);
		double mx=(this.b-this.a+.5)/(this.c-1);
		double npa=(b-.5-this.a)/(this.c);
		double npb=(b+.5-this.a)/(this.c);
		if(npb>=mn&&npa<=mx){
			this.b=b;
			this.c++;
			this.t+=this.s*r[b];
			this.s=-1*this.s;
			if(Midi02.cx<=this.c){
				if(Midi02.hs.get(this)==null){
					Midi02.hs.put(this,1);
					Midi02.cs.add(this);
					//U.imp(Midi02.cs+"");
					//System.exit(0);
				}
			}
			if(this.c>Midi02.dep){
				Midi02.dep=this.c;
				Midi02.cdep=this;
			}
			return true;
		}
		return false;
	}
	public String toString(){
		return this.a+"<>"+this.b+":"+c+":f="+(22050./((this.b-this.a*1.)/this.c))+"-->"+(this.t/this.c);
	}
}
public class Midi02{
	public static HashMap<Integer,ArrayList<C>>hx=new HashMap<>();
	public static HashMap<C,Integer>hs=new HashMap<>();
	public static ArrayList<C>cs=new ArrayList<>();
	public static int cx=100;
	public static double tol=5000;
	public static int dep=0;
	public static C cdep=null;
	public static void add(double[]datos,int a,int b){
		if(hx.get(b)==null){
			hx.put(b,new ArrayList<>());
		}
		if(hx.get(a)!=null){
			for(int i=0;i<hx.get(a).size();i++){
				if(hx.get(a).get(i).add(a,b,datos)){
					hx.get(b).add(hx.get(a).get(i));
				}
			}
		}
		hx.get(b).add(new C(a,b,datos));
	}
	public static void main(String[]args){
		try{
			c.U.imp("Inicio");
			String nombre="D:\\Wavs\\a697276mx";
			//Wave w=new Wave(new File(nombre+".wav"));
			//double[]datos=w.getms();
			double[]datos=WGen.sinusoide(32000,100,44100);
			//Wave.p(datos,"D:\\Wavs\\Midi02temp.wav");
			int ni=68;
			int nf=70;
			int pmin=(int)Math.floor(22050./WGen.gf(nf));
			int pmax=(int)Math.ceil(22050./WGen.gf(ni));
			c.U.imp(pmin+":"+pmax);
			for(int i=0;i<datos.length;i++){
				for(int j=i+pmin;j<datos.length&&j<i+pmax;j++){
					add(datos,i,j);
				}
			}
			c.U.imp(""+dep);
			//c.U.imp(""+cs);
			double tmx=0;
			C cmx=null;
			c.U.imp("cs.size():"+cs.size());
			for(int i=0;i<cs.size();i++){
				if(Math.abs(cs.get(i).t/cs.get(i).c)>tmx){
					tmx=Math.abs(cs.get(i).t/cs.get(i).c);
					cmx=cs.get(i);
				}
			}
			c.U.imp(cmx+"");
			c.U.imp(cdep+"");
		}catch(Exception e){
			c.U.imp(e);
		}
	}
}