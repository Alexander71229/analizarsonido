import java.util.*;
import java.io.*;
import java.awt.*;
import java.awt.image.*;
import javax.imageio.*;
import static c.U.imp;
public class T001{
	public static void main(String[]argumentos){
		try{
			c.U.ruta="log.txt";
			imp("Inicio");
			String nombre="T001";
			int ni=28;
			int nf=120;
			int dt=0;
			int na=1;
			int ln=100;
			double[]d=new Wave(new File(nombre+".wav")).getms();
			ArrayList<M>m=M.rango(ni,nf,dt,na);
			double[][]xms=new double[m.size()][ln];
			for(int i=0;i<m.size();i++){
				m.get(i).equc();
				m.get(i).res();
				for(int j=0;j<(44100./m.get(i).f);j++){
					m.get(i).act(d[j]);
				}
				imp(m.get(i).n+">"+(m.get(i).c*m.get(i).m));
				for(int k=0;k<ln;k++){
					xms[i][k]=m.get(i).c*m.get(i).m;
				}
			}
			ImageIO.write(SpecPrint.gen(xms,ni,nf,dt),"png",new File(nombre+"_T001.png"));
			imp("Fin");
		}catch(Throwable t){
			imp(t);
		}
	}
}