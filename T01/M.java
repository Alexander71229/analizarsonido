import java.util.*;
public class M{
	public int n;
	public double c;
	public double f;
	public double w;
	public double x;
	public double v;
	public double m;
	public static double coe=0.999;
	public double cx=0;
	public double tmm=44100.;
	public M(double f){
		c=1.;
		setF(f);
		this.cx=M.coe;
	}
	public M(int n,double f){
		c=1.;
		setF(f);
		this.n=n;
		this.cx=M.coe;
	}
	public void setF(double f){
		this.f=f;
		this.w=4*Math.PI*Math.PI*this.f*this.f;
		this.n=(int)Math.round(WGen.gn(this.f));
	}
	public void res(){
		this.x=0;
		this.v=0;
		this.m=0;
	}
	public void act(double d){
		v=(d-x)*w/this.tmm+v;
		x=(v/this.tmm+x)*cx;
		if(m<Math.abs(x))m=Math.abs(x);
	}
	public static double gf(int n,int j,int dt){
		int tc=dt*2+1;
		return 440*Math.pow(2.,((n-69.)+j/(tc*1.))/12.);
	}
	public static ArrayList<M>rango(int ni,int nf,int dt){
		ArrayList<M>resultado=new ArrayList<M>();
		for(int n=ni;n<=nf;n++){
			for(int j=-dt;j<=dt;j++){
				resultado.add(new M(n,M.gf(n,j,dt)));
			}
		}
		return resultado;
	}
	//Armónicos
	public static ArrayList<M>rango(int ni,int nf,int dt,int na){
		ArrayList<M>resultado=new ArrayList<M>();
		for(int n=ni;n<=nf;n++){
			for(int j=-dt;j<=dt;j++){
				for(int i=1;i<=na;i++){
					resultado.add(new M(i*M.gf(n,j,dt)));
				}
			}
		}
		return resultado;
	}
	public void equc(){
		double p=44100./f;
		int l=(int)Math.round(p);
		double[]d=WGen.sinusoide(30000.,p,l);
		for(int j=0;j<d.length;j++){
			act(d[j]);
		}
		c=10000./m;
	}
}