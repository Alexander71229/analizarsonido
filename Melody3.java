import java.util.*;
import java.io.*;
import java.awt.*;
import java.awt.image.*;
import javax.imageio.*;
public class Melody3{
	public static ArrayList<int[]>gd(File f)throws Exception{
		ArrayList<int[]>r=new ArrayList<>();
		Scanner s=new Scanner(f);
		while(s.hasNextLine()){
			String l=s.nextLine().replaceAll("\\.","");
			if(l.isEmpty()){
				continue;
			}
			String[]pl=l.split(" ");
			r.add(new int[]{Integer.parseInt(pl[0]),Integer.parseInt(pl[1])});
		}
		return r;
	}
	public static void main(String[]args){
		try{
			c.U.imp("Inicio");
			String nombre="C:\\Desarrollo\\Audio\\wavs\\Silent02";
			Wave w=new Wave(new File(nombre+".wav"));
			ArrayList<int[]>r=gd(new File("Silent02.mel"));
			Midiw md=new Midiw(nombre+".mid");
			double[]datos=w.getms();
			double[]s=new double[datos.length];
			int dt=100;
			int na=1;
			int k=0;
			Masa.coe=0.995;
			//Masa.coe=1;
			ArrayList<Masa>nz=Masa.rango(r.get(0)[1],r.get(0)[1],0,na);
			int n=-1;
			int an=-1;
			md.ini(r.get(k)[1],70,0);
			for(int i=0;i<datos.length;i++){
				if(k<r.size()-1&&i>=r.get(k+1)[0]){
					k++;
					md.fin(r.get(k-1)[1],i);
					md.ini(r.get(k)[1],70,i);
					if(r.get(k)[1]>0){
						for(int j=1;j<=na;j++){
							nz.get(j-1).setF(j*WGen.gf(r.get(k)[1]));
						}
					}
				}
				if(r.get(k)[1]>0){
					s[i]=Masa.g(nz,datos[i]);
				}else{
					s[i]=Masa.g(nz,0);
				}
			}
			md.fin(r.get(r.size()-1)[1],datos.length);
			Wave.p(WGen.sumar(s,1,datos,1,0),nombre+"-Melody3"+dt+"_"+na+".wav");
			//Wave.p(s,nombre+"-Melody3"+dt+"_"+na+".wav");
			md.write();
			c.U.imp("Fin");
		}catch(Exception e){
			c.U.imp(e);
		}
	}
}