import java.util.*;
import java.io.*;
import java.awt.*;
import java.awt.image.*;
import javax.imageio.*;
import static c.U.imp;
public class Test04{
	public static void main(String[]args){
		try{
			Wave w=new Wave(new File("Cabeza.wav"));
			Midiw m=new Midiw("Test04");
			double[]datos=w.getms();
			double dt=Math.pow(2,1./(12.*10))-1.;
			c.U.imp("dt:"+dt);
			int l=10000;
			double tol=100;
			//Masa.coe=0.99;
			ArrayList<Masa>masa=Masa.rangoValidacion(48,72+24,dt);
			for(int i=0;i<datos.length;i++){
				if(i%l==0&&i>0){
					for(int j=1;j<masa.size();j=j+3){
						if(masa.get(j).m>masa.get(j+1).m+tol&&masa.get(j).m>masa.get(j-1).m+tol){
							m.add(masa.get(j).n,masa.get(j).m,i-l,i);
						}
					}
					for(int j=0;j<masa.size();j++){
						masa.get(j).res();
					}
				}
				for(int j=0;j<masa.size();j++){
					masa.get(j).act(datos[i]);
				}
			}
			m.write(80);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}