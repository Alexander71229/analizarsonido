import java.util.ArrayList;
public class Fourier{
	public static ArrayList<ArrayList<Double>>sinusoide(double[]datos,ArrayList<Integer>periodos){
		ArrayList<ArrayList<Double>>amplitudes=new ArrayList<ArrayList<Double>>();
		double[]totales=new double[periodos.size()];
		for(int i=0;i<periodos.size();i++){
			amplitudes.add(new ArrayList<Double>());
		}
		for(int t=0;t<datos.length;t++){
			for(int i=0;i<periodos.size();i++){
				totales[i]+=Math.sin(2.*Math.PI*t/periodos.get(i))*datos[t];
				if(t%periodos.get(i)==0&&t>0){
					amplitudes.get(i).add(2.*totales[i]/periodos.get(i));
					totales[i]=0;
				}
			}
		}
		return amplitudes;
	}
	public static ArrayList<ArrayList<Double>>sinusoidePendiente(double[]datos,ArrayList<Integer>periodos){
		ArrayList<ArrayList<Double>>amplitudes=new ArrayList<ArrayList<Double>>();
		double[]totales=new double[periodos.size()];
		double[]totalesAnteriores=new double[periodos.size()];
		for(int i=0;i<periodos.size();i++){
			amplitudes.add(new ArrayList<Double>());
		}
		for(int t=0;t<datos.length;t++){
			for(int i=0;i<periodos.size();i++){
				int desfase=periodos.get(i);
				totales[i]+=Math.sin(2.*Math.PI*t/periodos.get(i))*datos[t];
				if(t>=desfase){
					totalesAnteriores[i]+=Math.sin(2.*Math.PI*(t-desfase)/periodos.get(i))*datos[(t-desfase)];
				}
				if(t%periodos.get(i)==0&&t>0){
					amplitudes.get(i).add((totales[i]-totalesAnteriores[i])/desfase);
				}
			}
		}
		return amplitudes;
	}
	public static double vc(ArrayList<int[]>d,int p,int a,int b){
		double r=0;
		for(int t=a;t<b;t++){
			r+=Math.cos(2.*Math.PI*(d.get(t)[0]-d.get(a)[0])/p)*d.get(t)[1];
		}
		return 2*r/(b-a);
	}
}