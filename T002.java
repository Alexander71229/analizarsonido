import java.util.*;
import java.io.*;
import static c.U.imp;
public class T002{
	public static void main(String[]argumentos){
		try{
			c.U.ruta="log.txt";
			imp("Inicio");
			String nombre="H:\\Desarrollo\\Audio\\Coro\\ProLírica\\Carmen\\Mp3\\TTS\\S02";
			double[]datos=new Wave(new File(nombre+".wav")).getms();
			double[]r=new double[datos.length];
			int ni=57;
			int nf=57;
			int dt=0;
			int na=20;
			Masa.coe=0.995;
			ArrayList<Masa>m=Masa.rango(ni,nf,dt,na);
			ArrayList<Masa>z=Masa.rango(ni,nf,dt,na);
			int[]x=new int[m.size()];
			int[]a=new int[m.size()];
			for(int i=0;i<x.length;i++){
				x[i]=1;
			}
			for(int t=0;t<datos.length;t++){
				for(int i=0;i<m.size();i++){
					m.get(i).act(datos[t]);
					if(t>=x[i]*44100./m.get(i).f){
						z.get(i).v=m.get(i).m;
						z.get(i).x=0;
						WGen.act0(r,z.get(i),a[i],t);
						a[i]=t;
						x[i]++;
						m.get(i).m=0;
					}
				}
			}
			Wave.p(r,nombre+"-T002.wav");
			imp("Fin");
		}catch(Throwable t){
			imp(t);
		}
	}
}