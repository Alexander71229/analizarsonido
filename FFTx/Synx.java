import java.io.*;
public class Synx{
	public double[]anx(double[]d,int ia,int ib){
		int a=-1;
		int b=-1;
		double[]dx=new double[d.length];
		for(int t=0;t<d.length-1;t++){
			if((d[t]<=0.&&d[t+1]>0.)||(d[t]>=0.&&d[t+1]<0.)){
				if(a<0){
					a=t;
				}else{
					//System.out.println(t-a);
					if(t-a>=ia&&t-a<=ib){
						b=t;
					}
				}
				if(a>=0&&b>0){
					dft(d,a,b,dx);
					a=b;
					b=-1;
				}
			}
		}
		return dx;
	}
	public void dft(double[]o,int a,int b,double[]d){
		int n=20;
		for(int i=1;i<=n;i++){
			double x=0;
			for(int j=a;j<b;j++){
				x+=o[j]*Math.sin(2*Math.PI*(j-a)*i/(b-a));
			}
			x=2*x/(b-a);
			for(int j=a;j<b;j++){
				d[j]+=x*Math.sin(2*Math.PI*(j-a)*i/(b-a));
			}
			x=0;
			for(int j=a;j<b;j++){
				x+=o[j]*Math.cos(2*Math.PI*(j-a)*i/(b-a));
			}
			x=2*x/(b-a);
			for(int j=a;j<b;j++){
				d[j]+=x*Math.cos(2*Math.PI*(j-a)*i/(b-a));
			}
		}
	}
	public static void main(String[]args){
		try{
			int l=44100;
			double[]d=new double[l];
			for(int t=0;t<l;t++){
				d[t]=32000.*Math.sin(2.*Math.PI*t/100.)+100.*Math.sin(4.*Math.PI*t/100.);
			}
			Synx s=new Synx();
			Wave w=new Wave(new File("Test2.wav"));
			d=w.getms();
			Wave.p(s.anx(d,280,500),"Out.wav");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}