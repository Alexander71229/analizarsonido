import java.io.*;
import java.util.*;
public class Ciclo03{
	public static void main(String[]argumentos){
		try{
			System.out.println("Inicio");
			c.U.imp("Inicio");
			String nombre="D:\\Wavs\\Seek and Destroy-Bass2";
			Wave w=new Wave(new File(nombre+".wav"));
			double[]datos=w.getms(0,44100*30);
			Midiw m=new Midiw(nombre);
			m.sa=new int[0];
			int ni=76;
			int nf=76;
			int dt=0;
			int na=1;
			ArrayList<Masa>masa=Masa.rango(ni,nf,dt,na);
			Masa.coe=0.995;
			double max=0;
			for(int k=1;k<=2;k++){
				for(int i=0;i<datos.length;i++){
					double t=0;
					for(int j=0;j<masa.size();j++){
						masa.get(j).act(datos[i]);
						t=t+masa.get(j).x*masa.get(j).f;
					}
					datos[i]=t;
					if(t>max){
						max=t;
					}
				}
			}
			//Wave.p(datos,nombre+"-c3a.wav");
			double[]d=new double[datos.length];
			ArrayList<int[]>a=WGen.maximos(datos,max);
			/*for(int i=0;i<a.size();i++){
				d[a.get(i)[0]]=a.get(i)[1];
			}
			Wave.p(d,nombre+"-c3b.wav");*/
			ArrayList<int[]>b=WGen.maximos(a);
			ArrayList<int[]>actual=null;
			for(int i=0;i<b.size()-2;i++){
				if(actual!=null&&b.get(i)[0]>=actual.get(actual.size()-1)[0]){
					c.U.imp(WGen.gn(actual)+":"+actual.get(0)[0]+"-->"+actual.get(actual.size()-1)[0]);
					m.add(WGen.gn(actual),20000,actual.get(0)[0],actual.get(actual.size()-1)[0]);
					actual=null;
				}
				ArrayList<int[]>x1=WGen.ciclos(b,i,i,i+1);
				ArrayList<int[]>x2=WGen.ciclos(b,i,i,i+2);
				if(actual==null||WGen.longitud(x1)>WGen.longitud(actual)){
					if(x1.size()>0&&WGen.gn(x1)>=ni&&WGen.mayor(x1,5000)){
						actual=x1;
					}
				}
				if(actual==null||WGen.longitud(x2)>WGen.longitud(actual)){
					if(x2.size()>0&&WGen.gn(x2)>=ni&&WGen.gn(x1)-WGen.gn(x2)!=12&&WGen.mayor(x2,5000)){
						actual=x2;
						if(Math.round(WGen.gn(b,x2,i))==Math.round(WGen.gn(x2)+12)){
							actual=WGen.ciclos(b,x2,i);
						}
					}
				}
			}
			m.write();
		}catch(Throwable t){
			c.U.imp(t);
		}
	}
}