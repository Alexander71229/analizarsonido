import java.util.*;
import java.io.*;
import java.awt.*;
import java.awt.image.*;
import javax.imageio.*;
public class Test09{
	public static void main(String[]args){
		try{
			double[]datos=new Wave(new File("Sweet.wav")).getms();
			ArrayList<Masa>m=Masa.rango2(59,70,0);
			int tm=2*2205;
			//int l=tm*1*1830;
			int l=datos.length;
			Masa.coe=1;
			int lx=1;
			double[][]xms=new double[m.size()][lx*l/tm];
			double mx=0;
			double mn=Double.MAX_VALUE;
			int rep=0;
			int o=rep;
			for(int t=0;t<l&&t<datos.length;t++){
				if(t>0&&t%tm==0){
					if(o>0){
						t=t-tm;
						o--;
						continue;
					}
					o=rep;
					for(int i=0;i<m.size();i++){
						//m.get(i).m=m.get(i).m/m.get(i).f;
						if(mx<m.get(i).m){
							mx=m.get(i).m;
						}	
						if(mn>m.get(i).m){
							mn=m.get(i).m;
						}
						for(int k=0;k<lx;k++){
							xms[i][(t/tm-1)*lx+k]=m.get(i).m;
						}
						m.get(i).res();
					}
				}
				for(int i=0;i<m.size();i++){
					m.get(i).act(datos[t]);
				}
			}
			ImageIO.write(SpecPrint.gen(xms),"png",new File("Test09.png"));
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}